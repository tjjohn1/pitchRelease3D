var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var opn = require('opn');

var app = express();
port = process.env.PORT || 3000;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/assets', express.static(path.join(__dirname+'/assets')));
var routes = require('./routes/router');
routes(app);

app.get('/', function (req, res) { res.sendFile(path.join(__dirname+'/assets/index.html')); });

app.get('/json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(json));
});

module.exports = app;

app.listen(port);

console.log('RESTful API server started on: ' + port);
console.log('Opening chrome browser to game_pitches_3 based pitchRelease visualizer');

opn('http://localhost:3000', {app: ['chrome']});
