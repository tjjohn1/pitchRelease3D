let pitchRelData = {
    timeout:{},
    header:{},
    $loading: {},
    yPosHalf:"",
    endDate:"",
    startDate:"",
    league:"",
    pitchersAvailable:{},
    pitcherTeams:{},
    allTeams:[],
    pitchersPerTeam:{},
    pitchTypesPerPitcher:{},
    selectedPitcherId:0,
    selectedPitcher:"",
    selectedTeam:"",
    pitchersValues:{},
    pitcherValues:{},
    isInitial:true,
    avgsSelected:false,
    pitBrkdwnSelected:false,
    allPitchersSelected:false,
    allPitches:{},
    calculatedMaps:{
        'eachPitchForIndividual':{},
        'totsAvgAllPitchersPitBrkdwn':{},
        'totsAvgOnePitcherPitBrkdwn':{},
        'totsAvgAllPitchersNoPitBrkdwn':{},
        'totsAvgOnePitcherNoPitBrkdwn':{}
    },
    leaguesAvailable: {
        'MLB': {
            'name': 'Major League',
            'code': 'MLB',
            'abbreviation': 'MLB'
        },
        'AAA': {
            'name': 'Triple-A',
            'code': 'AAA',
            'abbreviation': 'AAA'
        },
        'AA': {
            'name': 'Double-A',
            'code': 'AA',
            'abbreviation': 'AA'
        },
        'A+': {
            'name': 'Advanced-A',
            'code': 'A+',
            'abbreviation': 'A+'
        },
        'A': {
            'name': 'Single-A',
            'code': 'A',
            'abbreviation': 'A'
        },
        'SS': {
            'name': 'Short Season',
            'code': 'SS',
            'abbreviation': 'SS'
        },
        'RK': {
            'name': 'Rookie',
            'code': 'RK',
            'abbreviation': 'RK'
        }
    },

    ascii_to_hexa: function(str)
    {
        let arr1 = [];
        for (let n = 0, l = str.length; n < l; n ++)
        {
            let hex = Number(str.charCodeAt(n)).toString(16);
            arr1.push(hex);
        }
        return arr1.join('');
    },

    isArray: function(what) {
        return Object.prototype.toString.call(what) === '[object Array]';
    },

    isJson: function(item) {
        item = typeof item !== "string"
            ? JSON.stringify(item)
            : item;

        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        return typeof item === "object" && item !== null;
    },

    stringReplace: function(str,replaceWhat,replaceTo) {
        replaceWhat = replaceWhat.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        let re = new RegExp(replaceWhat, 'g');
        return str.replace(re, replaceTo);
    },

    parseDate: function(str) {
        //var mdy = str.split('/');
        return new Date(str);
    },

    datediff: function(first, second) {
        console.log("first:");
        console.log(first);
        console.log("second:");
        console.log(second);
        // Take the difference between the dates and divide by milliseconds per day.
        // Round to nearest whole number to deal with DST.
        return Math.round((Math.abs(second-first))/(1000*60*60*24));
    },

    initFirstPhase: function(){
        $(document)
            .ajaxStart(function () {
                pitchRelData.$loading.show();
            });

        /*
        .ajaxStop(function () {
            statsAPIpitches.$loading.hide();
        });
        */
        pitchRelData.$loading = $('#loadingDiv').hide();
        pitchRelData.header = $('<div id="header" style="display:block;"></div>')
            .insertBefore('#loadingDiv');
        let dateRangeInputTitle = $('<div id="date-range-title" class="pitchReleaseThreeD-font pitchReleaseThreeD-title">Select date range and level to retrieve pitches</div>');
        let dateRangeInputParent = $('<div id="date-range-input-parent" class="pitchReleaseThreeD-font pitchReleaseThreeD-selector-parent"></div>');
        pitchRelData.header
            .append(dateRangeInputTitle)
            .append(dateRangeInputParent);
        pitchRelData.yPosHalf = $('#date-range-title').height();
        $('#date-range-input-parent')
            .append('<div id="league-select-div" class="pitchReleaseThreeD-selector-div"><div class="pitchReleaseThreeD-label threeDpitch-font">Level</div><select id="league-select" class="pitchReleaseThreeD-selector pitchReleaseThreeD-shadowed"></select></div>')
            .append('<div class="pitchReleaseThreeD-selector-div" style="margin-left:10px"><div class="pitchReleaseThreeD-label pitchReleaseThreeD-font">Start Date</div><input id="pitchReleaseThreeD-start-date" class="pitchReleaseThreeD-font pitchReleaseThreeD-selector pitchReleaseThreeD-shadowed" autocomplete="off"></div>')
            .append('<div class="pitchReleaseThreeD-selector-div"><div class="pitchReleaseThreeD-label pitchReleaseThreeD-font">End Date</div><input id="pitchReleaseThreeD-end-date" class="pitchReleaseThreeD-font pitchReleaseThreeD-selector pitchReleaseThreeD-shadowed" autocomplete="off"></div>');
        $( "#pitchReleaseThreeD-end-date" ).datepicker({
            dateFormat: "mm-dd-yy",
            changeMonth: true,
            changeYear: true,
            autoSize: true,
            maxDate: 0,
            minDate: "-2y",
            showOn: "focus",
            showAnim: "fold",
            onSelect: function(dateText) {
                let dateEnd = $('#pitchReleaseThreeD-end-date').datepicker('getDate');
                let newEndDate = new Date(dateEnd);
                newEndDate.setDate(dateEnd.getDate() - 7);
                let endDateMinus7 = new Date(newEndDate);
                pitchRelData.endDate = dateText;
                $("#threeDpitch-start-date")
                    .datepicker('option', 'minDate', endDateMinus7)
                    .datepicker('option', 'maxDate', pitchRelData.endDate );
                //console.log("End Date: " + dateEnd);
                //console.log("End Date minus 7: " + endDateMinus7);
                if ((pitchRelData.startDate != null && pitchRelData.startDate !== "") && (pitchRelData.league !== null && pitchRelData.league !== "")) {
                    pitchRelData.$loading.show();
                    pitchRelData.retrievePitches(pitchRelData.startDate, pitchRelData.endDate, pitchRelData.league);
                }
            },
            setDate: null
        });
        //$( "#threeDpitch-end-date" ).datepicker("setDate", 0);
        $( "#pitchReleaseThreeD-start-date" ).datepicker({
            dateFormat: "mm-dd-yy",
            changeMonth: true,
            changeYear: true,
            autoSize: true,
            maxDate: 0,
            minDate: "-2y",
            showOn: "focus",
            showAnim: "fold",
            onSelect: function(dateText) {
                let dateStart = $('#pitchReleaseThreeD-start-date').datepicker('getDate');
                let newStartDate = new Date(dateStart);
                newStartDate.setDate(dateStart.getDate() + 7);
                let startDatePlus7 = new Date(newStartDate);
                pitchRelData.startDate = dateText;
                $("#pitchReleaseThreeD-end-date")
                    .datepicker('option', 'minDate', pitchRelData.startDate )
                    .datepicker('option', 'maxDate', startDatePlus7 );
                //console.log("Start Date: " + dateStart);
                //console.log("Start Date plus 7: " + startDatePlus7);
                if ((pitchRelData.endDate != null && pitchRelData.endDate !== "") && (pitchRelData.league !== null && pitchRelData.league !== "")) {
                    pitchRelData.$loading.show();
                    pitchRelData.retrievePitches(pitchRelData.startDate, pitchRelData.endDate, pitchRelData.league);
                }
            },
            setDate: null
        });
        let leagueSelect = $( "#league-select" );
        $.each(pitchRelData.leaguesAvailable, function (key, value) {
            leagueSelect
                .append('<option value="' + key + '">' + value['name'] + '</option>');
        });
        leagueSelect
            .change(function(){
                pitchRelData.league = $(this).val();
                //console.log("League selected: " + statsAPIpitches.league);
                if ((pitchRelData.endDate != null && pitchRelData.endDate !== "") && (pitchRelData.startDate != null && pitchRelData.startDate !== "")) {
                    pitchRelData.$loading.show();
                    pitchRelData.retrievePitches(pitchRelData.startDate, pitchRelData.endDate, pitchRelData.league);
                }
            });

        leagueSelect.prop('selectedIndex',-1);
        leagueSelect.select2_405();
        leagueSelect.next("span").addClass("pitchReleaseThreeD-shadowed").css("width", "140px");
        $('.select2_405-container, .select2_405-selection--single, .select2_405-selection__rendered').css('color', '#222');
        //$( "#threeDpitch-start-date" ).datepicker("setDate", "-1m");
    },

    retrievePitches: async function(startDate, endDate, level){
        $('#header').fadeOut( async function(){
            $('#header').empty();
            pitchRelData.getStoredJSON(startDate, endDate, level);
        });
    },

    clearAll: function(){
        pitchRelData.calculatedMaps.eachPitchForIndividual = {};
        pitchRelData.calculatedMaps.totsAvgAllPitchersPitBrkdwn = {};
        pitchRelData.calculatedMaps.totsAvgOnePitcherPitBrkdwn = {};
        pitchRelData.calculatedMaps.totsAvgAllPitchersNoPitBrkdwn = {};
        pitchRelData.calculatedMaps.totsAvgOnePitcherNoPitBrkdwn = {};
        pitchRelData.allPitches = {};
        pitchRelData.pitchersAvailable = {};
        pitchRelData.pitchersPerTeam = {};
        pitchRelData.allTeams = [];
        pitchRelData.pitchTypesPerPitcher = {};
        pitchRelData.endDate = "";
        pitchRelData.startDate = "";
        pitchRelData.league = "";
    },

    reInit: function(){
        pitchRelData.$loading = $('#loadingDiv').show();
        Promise.all(
            [
                $('#header').fadeOut(function () {
                    $('#header').empty(); }).promise(),
                $('.pitchReleaseThreeD-back-div').fadeOut(function () {
                    $('.pitchReleaseThreeD-back-div').empty(); }).promise(),
                $('#containerDiv').fadeOut(function () {
                    $('#containerDiv').empty(); }).promise()
            ]
        ).then(async function () {
            let promise = new Promise((resolve, reject) => {
                resolve();
            });
            if(!pitchRelData.isInitial){
                pitchReleaseThreeD.cleanUp();
            }
            $('#header').remove();
            $('.pitchReleaseThreeD-back-div').remove();
            $('#containerDiv').remove();
            await(promise)
            //console.log('all done');
            pitchRelData.isInitial = true;
            pitchRelData.initFirstPhase();
        });
    },

    initSecondPhase: async function(){

        $('<div class="pitchReleaseThreeD-back-div"><img id="date-range-return" class="pitchReleaseThreeD-back-arrow pitchReleaseThreeD-light-shadowed" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAAAxlBMVEUAAABpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWl+xKtIAAAAQXRSTlMAAQIDBAUGCAkLDQ4PGRwdICQzNzg8PUJKTE1OUFZcXV9iZHuAgoiJj56jqK2vsLW6vMrOz9XZ3ODk6evv8/n7/Vt3xbQAAADpSURBVEjH7dXJUsJAFIXhk5AQZoygjA44gQoyKDMq5/1fygUUJVTRwOkl+fffpuve20BU1BkXODJNvHLiadSp/5AsSDY7IElWBBp75Krq6fZyRhUn30kROze/VHHuixSx98zt2vfryv4hW5xzbyPXSNMdmsqbHupuabQM99uLISliv0mq+HpBFQddUsYdWuC+DU70LDBQ+rbAiLcsMBCOLDCchj6eADJdeTEA4MowaWP30EL7Lzvk42FdLX7ELdnZrlMP4O1Sx0CqbYH/3zIBw3uywEDuU/7oNj9HEWLJN059yAUuoqLOqj8TO7g70Ps42AAAAABJRU5ErkJggg=="><div class="threeDpitch-back-label threeDpitch-font">Back to date range</div></div>')
            .insertBefore('#loadingDiv');
        $('#date-range-return')
            .mousedown(function(){
                $('#date-range-return').removeClass("pitchReleaseThreeD-light-shadowed");
            })
            .mouseout(function(){
                $('#date-range-return').addClass("pitchReleaseThreeD-light-shadowed");
            })
            .mouseup(function(){
                $('#date-range-return').addClass("pitchReleaseThreeD-light-shadowed");
            })
            .click(function(){
                pitchRelData.clearAll();
                pitchRelData.reInit();
            });

        let targetDiv = $('<div id="targetDiv" style="position:fixed; top:120px; left: calc(100% - 235px); display:none"><div class="pitchReleaseThreeD-font" style="font-size: 1.1em; font-weight:bold; margin-right:10px; display:inline-block; vertical-align: top">Target Pitcher: </div><div style="height:1em; width: 1em; background-color: #39ff14; display:inline-block; border-radius:5px; border: #212121 1px ridge ;vertical-align: center"></div></div>');
        let avgToggleOuterDiv = $('<div style="position:fixed; top:15px; left: calc(100% - 365px);"></div>');
        let avgToggleTitleDiv = $('<div style="font-weight: bold; font-size: 20px; color: #414141" class="pitchRelease3D-font">&nbsp&nbsp&nbsp&nbsp&nbsp<br>&nbspAverages</div>');
        avgToggleOuterDiv.append(avgToggleTitleDiv);
        let avgToggleInnerDiv = $('<div style="display:inline-block; height:1px; margin-left:10px; padding:0; "></div>');
        avgToggleInnerDiv.append('<label class="switch" style="display:inline-block"><input id="avgToggle" type="checkbox" class="checkbox" style="height: 1px;margin:0; display:none"/><div class=""></div></label>');
        avgToggleOuterDiv.append(avgToggleInnerDiv);

        let allPitchersToggleOuterDiv = $('<div id="allPitchersToggleParent" style="position:fixed; top:15px; left: calc(100% - 245px)"></div>');
        let allPitchersToggleTitleDiv = $('<div style="font-weight: bold; font-size: 20px; color: #414141" class="pitchRelease3D-font">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAll<br>&nbsp&nbspPitchers</div>');
        allPitchersToggleOuterDiv.append(allPitchersToggleTitleDiv);
        let allPitchersToggleInnerDiv = $('<div style="display:inline-block; height:1px; margin-left:10px; padding:0; "></div>');
        allPitchersToggleInnerDiv.append('<label class="switch" style="display:inline-block"><input id="allPitchersToggle" type="checkbox" class="checkbox" style="height: 1px;margin:0; display:none"/><div class=""></div></label>');
        allPitchersToggleOuterDiv.append(allPitchersToggleInnerDiv);

        let byPitchToggleOuterDiv = $('<div id="byPitchToggleParent" style="position:fixed; top:15px; left: calc(100% - 125px);"></div>');
        let byPitchToggleTitleDiv = $('<div style="font-weight: bold; font-size: 20px; color: #414141" class="pitchRelease3D-font">&nbsp&nbsp&nbspPitch<br>Breakdown</div>');
        byPitchToggleOuterDiv.append(byPitchToggleTitleDiv);
        let byPitchToggleInnerDiv = $('<div style="display:inline-block; height:1px; margin-left:10px; padding:0; "></div>');
        byPitchToggleInnerDiv.append('<label class="switch" style="display:inline-block"><input id="byPitchToggle" type="checkbox" class="checkbox" style="height: 1px;margin:0; display:none"/><div class=""></div></label>');
        byPitchToggleOuterDiv.append(byPitchToggleInnerDiv);

        pitchRelData.header
            .append(avgToggleOuterDiv)
            .append(allPitchersToggleOuterDiv)
            .append(byPitchToggleOuterDiv);

        let selectorParent = $('<div id="all-select-parent" class="pitchReleaseThreeD-font pitchReleaseThreeD-selector-parent"></div>');
        let staticTitle = $('<div id="all-title" class="pitchRelease3D-font pitchReleaseThreeD-title">'+pitchRelData.league+' Pitches '+ pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/") +'</div>');
        let changingTitle = $('<div id="player-select-title" class="pitchRelease3D-font pitchReleaseThreeD-title-sub">Select target pitcher</div>');
        pitchRelData.header
            .append(staticTitle)
            .append(changingTitle)
            .append(targetDiv)
            .append(selectorParent);

        let teamSelectParent = $('<div id="team-select-div" class="pitchReleaseThreeD-selector-div" style="width:125px"></div>');
        let teamSelectTitle = $('<div class="pitchReleaseThreeD-label pitchRelease3D-font" >Team</div>');
        let teamSelect = $('<select id="team-select" class="pitchReleaseThreeD-selector pitchReleaseThreeD-shadowed"></select>');
        teamSelectParent
            .append(teamSelectTitle)
            .append(teamSelect);

        let pitcherSelectParent = $('<div id="pitcher-select-div" class="pitchReleaseThreeD-selector-div" style="width:300px"></div>');
        let pitcherSelectTitle = $('<div class="pitchReleaseThreeD-label pitchRelease3D-font">Pitcher</div>');
        let pitcherSelect = $('<select id="pitcher-select" class="pitchReleaseThreeD-selector pitchReleaseThreeD-shadowed"></select>');
        pitcherSelectParent
            .append(pitcherSelectTitle)
            .append(pitcherSelect);

        let pitchTypeSelectParent = $('<div id="pitch-type-select-div" class="pitchReleaseThreeD-selector-div" style="width:75px"></div>');
        let pitchTypeSelectTitle = $('<div class="pitchReleaseThreeD-label pitchRelease3D-font">Pitch Type</div>');
        let pitchTypeSelect = $('<select id="pitch-type-select" class="pitchReleaseThreeD-selector pitchReleaseThreeD-shadowed"></select>');
        pitchTypeSelectParent
            .append(pitchTypeSelectTitle)
            .append(pitchTypeSelect);

        selectorParent
            .append(teamSelectParent)
            .append(pitcherSelectParent)
            .append(pitchTypeSelectParent);

        for(let i = 0; i < pitchRelData.allTeams.length; i++){
            let team = pitchRelData.allTeams[i];
            teamSelect
                .append('<option value="' + team + '">' + team + '</option>');
        }
        teamSelect.prop('selectedIndex',-1);
        teamSelect.select2_405();
        pitcherSelect.select2_405();
        pitchTypeSelect.select2_405();
        teamSelect.next("span").addClass("pitchReleaseThreeD-shadowed").css("width", "125px");
        $('.select2_405-container, .select2_405-selection--single, .select2_405-selection__rendered').css('color', '#222');

        $('#avgToggle').change(function(){
            
            let allPitchersToggle = $('#allPitchersToggle');
            let byPitchToggle = $('#byPitchToggle');
            //console.log("avg toggled");
            if($(this).prop("checked") == true) {
                $('#allPitchersToggleParent').show();
                $('#byPitchToggleParent').show();
                allPitchersToggle.prop('checked', true);
                byPitchToggle.prop('checked', true);
                staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                changingTitle.text('All pitchers with pitch breakdown');
                pitchRelData.avgsSelected = true;
                pitchRelData.pitBrkdwnSelected= true;
                pitchRelData.allPitchersSelected = true;
                $('#targetDiv').fadeIn();
                if(pitcherSelect.prop('selectedIndex') >= 0){
                    let selectedPitcherOption = $("#pitcher-select option:selected");
                    let pitcherId = selectedPitcherOption.val();
                    pitchRelData.selectedPitcherId = pitcherId;
                    pitchRelData.selectedPitcher = pitchRelData.pitchersAvailable[pitcherId];
                    if(!pitchRelData.isInitial) {
                        pitchReleaseThreeD.disposePitches();
                    }
                    if(pitchTypeSelect.prop('selectedIndex') >= 0){
                        let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                        let pitchType = selectedPitchTypeOption.val();
                        pitchRelData.initiateAvgsAllPitchersPitBrkdwn3D(pitchType);
                    }
                }
            } else {
                allPitchersToggle.prop('checked');
                byPitchToggle.prop('checked');
                $('#allPitchersToggleParent').hide();
                $('#byPitchToggleParent').hide();
                staticTitle.text(''+pitchRelData.league+' Pitches '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                pitchRelData.avgsSelected = false;
                pitchRelData.pitBrkdwnSelected= false;
                pitchRelData.allPitchersSelected = false;
                $('#targetDiv').fadeOut();
                if(pitcherSelect.prop('selectedIndex') >= 0){
                    let selectedPitcherOption = $("#pitcher-select option:selected");
                    let pitcherId = selectedPitcherOption.val();
                    pitchRelData.selectedPitcherId = pitcherId;
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                    changingTitle.html('&nbsp');
                    if(!pitchRelData.isInitial) {
                        pitchReleaseThreeD.disposePitches();
                    }
                    if(pitchTypeSelect.prop('selectedIndex') >= 0){
                        let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                        let pitchType = selectedPitchTypeOption.val();
                        pitchRelData.initiateIndividualSinglePitch3D(pitchType);
                    }
                }
            }
        });

        $('#allPitchersToggle').change(function(){
            let byPitchToggle = $('#byPitchToggle');
            //console.log("all pitchers toggled");
            if($(this).prop("checked") == true) {
                pitchRelData.avgsSelected = true;
                if(byPitchToggle.prop("checked") == true){
                    pitchRelData.pitBrkdwnSelected = true;
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('All pitchers with pitch breakdown');
                } else {
                    pitchRelData.pitBrkdwnSelected = false;
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('All pitchers');
                }
                pitchRelData.allPitchersSelected = true;
                $('#targetDiv').fadeIn();
                if(pitcherSelect.prop('selectedIndex') >= 0){
                    let selectedPitcherOption = $("#pitcher-select option:selected");
                    let pitcherId = selectedPitcherOption.val();
                    pitchRelData.selectedPitcherId = pitcherId;
                    pitchRelData.selectedPitcher = pitchRelData.pitchersAvailable[pitcherId];
                    if(!pitchRelData.isInitial) {
                        pitchReleaseThreeD.disposePitches();
                    }
                    if(pitchTypeSelect.prop('selectedIndex') >= 0){
                        let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                        let pitchType = selectedPitchTypeOption.val();
                        if(pitchRelData.pitBrkdwnSelected){
                            pitchRelData.initiateAvgsAllPitchersPitBrkdwn3D(pitchType);
                        } else {
                            pitchRelData.initiateAvgsAllPitchersNoPitBrkdwn3D(pitchType);
                        }

                    }
                }
            } else {
                pitchRelData.avgsSelected = true;
                if(byPitchToggle.prop("checked") == true){
                    pitchRelData.pitBrkdwnSelected = true;
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('With pitch breakdown');
                } else {
                    pitchRelData.pitBrkdwnSelected = false;
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.html('&nbsp');
                }
                pitchRelData.allPitchersSelected = false;
                $('#targetDiv').fadeOut();
                if(pitcherSelect.prop('selectedIndex') >= 0){
                    let selectedPitcherOption = $("#pitcher-select option:selected");
                    let pitcherId = selectedPitcherOption.val();
                    pitchRelData.selectedPitcherId = pitcherId;
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    //staticTitle.text(pitcher + ' ('+team+'): 10/14/18 - 10/18/18');
                    if(!pitchRelData.isInitial) {
                        pitchReleaseThreeD.disposePitches();
                    }
                    if(pitchTypeSelect.prop('selectedIndex') >= 0){
                        let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                        let pitchType = selectedPitchTypeOption.val();
                        if(pitchRelData.pitBrkdwnSelected){
                            staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                            changingTitle.text('Averages with pitch breakdown');
                            pitchRelData.initiateAvgsOnePitcherPitBrkdwn3D(pitchType);
                        } else {
                            staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                            changingTitle.text('Averages');
                            pitchRelData.initiateAvgsOnePitcherNoPitBrkdwn3D(pitchType);
                        }
                    }
                }
            }
        });

        $('#byPitchToggle').change(function(){
            let allPitchersToggle = $('#allPitchersToggle');
            //console.log("pitch breakdown toggled");
            if($(this).prop("checked") == true) {
                pitchRelData.avgsSelected = true;
                if(allPitchersToggle.prop("checked") == true){
                    pitchRelData.allPitchersSelected = true;
                    $('#targetDiv').fadeIn();
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('All pitchers with pitch breakdown');
                } else {
                    pitchRelData.allPitchersSelected = false;
                    $('#targetDiv').fadeOut();
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                }
                pitchRelData.pitBrkdwnSelected = true;
                if(pitcherSelect.prop('selectedIndex') >= 0){
                    let selectedPitcherOption = $("#pitcher-select option:selected");
                    let pitcherId = selectedPitcherOption.val();
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    pitchRelData.selectedPitcherId = pitcherId;
                    pitchRelData.selectedPitcher = pitchRelData.pitchersAvailable[pitcherId];
                    if(!pitchRelData.isInitial) {
                        pitchReleaseThreeD.disposePitches();
                    }
                    if(pitchTypeSelect.prop('selectedIndex') >= 0){
                        let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                        let pitchType = selectedPitchTypeOption.val();
                        if(pitchRelData.allPitchersSelected){
                            pitchRelData.initiateAvgsAllPitchersPitBrkdwn3D(pitchType);
                        } else {
                            staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                            changingTitle.text('Averages with pitch breakdown');
                            pitchRelData.initiateAvgsOnePitcherPitBrkdwn3D(pitchType);
                        }

                    }
                }
            } else {
                pitchRelData.avgsSelected = true;
                if(allPitchersToggle.prop("checked") == true){
                    pitchRelData.allPitchersSelected = true;
                    $('#targetDiv').fadeIn();
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('All pitchers');
                } else {
                    pitchRelData.allPitchersSelected = false;
                    $('#targetDiv').fadeOut();
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.html('&nbsp');
                }
                pitchRelData.pitBrkdwnSelected = false;
                if(pitcherSelect.prop('selectedIndex') >= 0){
                    let selectedPitcherOption = $("#pitcher-select option:selected");
                    let pitcherId = selectedPitcherOption.val();
                    pitchRelData.selectedPitcherId = pitcherId;
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    //staticTitle.text(pitcher + ' ('+team+'): 10/14/18 - 10/18/18');
                    if(!pitchRelData.isInitial) {
                        pitchReleaseThreeD.disposePitches();
                    }
                    if(pitchTypeSelect.prop('selectedIndex') >= 0){
                        let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                        let pitchType = selectedPitchTypeOption.val();
                        if(pitchRelData.allPitchersSelected){
                            staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                            changingTitle.text('All pitchers');
                            pitchRelData.initiateAvgsAllPitchersNoPitBrkdwn3D(pitchType);
                        } else {
                            staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                            changingTitle.text('Averages');
                            pitchRelData.initiateAvgsOnePitcherNoPitBrkdwn3D(pitchType);
                        }
                    }
                }
            }
        });

        teamSelect
            .change(function() {
                if(!pitchRelData.isInitial){
                    pitchReleaseThreeD.disposePitches();
                }
                let selectedTeamOption = $("#team-select option:selected");
                let team = selectedTeamOption.val();
                let pitcherIdsArr = pitchRelData.pitchersPerTeam[team];
                pitchRelData.selectedTeam = team;
                pitcherSelect
                    .find('option')
                    .remove();
                pitcherIdsArr.sort(function(a, b){
                    if(a < b) { return -1; }
                    if(a > b) { return 1; }
                    return 0;
                });
                for(let i=0;i<pitcherIdsArr.length;i++){
                    let pitcherId = pitcherIdsArr[i];
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    pitcherSelect
                        .append('<option value="' + pitcherId + '">' + pitcherId + ' - ' + pitcher + '</option>');
                }
                pitcherSelect.prop('selectedIndex',-1);
                pitcherSelect.select2_405({
                    'minimumResultsForSearch': '-1'
                });
                pitcherSelect.next("span").addClass("pitchReleaseThreeD-shadowed").css("width", "300px");
                pitchTypeSelect.prop('selectedIndex',-1);
                pitchTypeSelect
                    .find('option')
                    .remove();
                pitchRelData.selectedPitcherId = null;
            });

        pitcherSelect
            .change(function() {
                if(!pitchRelData.isInitial){
                    pitchReleaseThreeD.disposePitches();
                }
                let selectedPitcherOption = $("#pitcher-select option:selected");
                let pitcherId = selectedPitcherOption.val();
                let pitchTypesArr = [];
                pitchRelData.selectedPitcherId = pitcherId;
                if(pitchRelData.avgsSelected && pitchRelData.allPitchersSelected && pitchRelData.pitBrkdwnSelected){
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('All pitchers with pitch breakdown');
                } else if(pitchRelData.avgsSelected && !pitchRelData.allPitchersSelected && pitchRelData.pitBrkdwnSelected){
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                    changingTitle.text('Averages with pitch breakdown');
                } else if(pitchRelData.avgsSelected && !pitchRelData.allPitchersSelected && !pitchRelData.pitBrkdwnSelected){
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                    changingTitle.text('Averages');
                } else if(pitchRelData.avgsSelected && pitchRelData.allPitchersSelected && !pitchRelData.pitBrkdwnSelected){
                    staticTitle.text(''+pitchRelData.league+' Averages: '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/"));
                    changingTitle.text('All pitchers');
                } else if(!pitchRelData.avgsSelected){
                    let pitcher = pitchRelData.pitchersAvailable[pitcherId];
                    let team = pitchRelData.pitcherTeams[pitcherId];
                    staticTitle.text(pitcher + ' ('+team+'): '+pitchRelData.stringReplace(pitchRelData.startDate,"-","/")+' - '+pitchRelData.stringReplace(pitchRelData.endDate,"-","/")+'');
                    changingTitle.html('&nbsp');
                }
                $.each(pitchRelData.pitchTypesPerPitcher[pitcherId], function (key) {
                    pitchTypesArr.push(key);
                });
                pitchTypeSelect
                    .find('option')
                    .remove();
                pitchTypesArr.sort(function(a, b){
                    if(a < b) { return -1; }
                    if(a > b) { return 1; }
                    return 0;
                });
                pitchTypeSelect
                    .append('<option value="ALL">ALL</option>');
                for(let i=0;i<pitchTypesArr.length;i++){
                    pitchTypeSelect
                        .append('<option value="' + pitchTypesArr[i] + '">' + pitchTypesArr[i] + '</option>');
                }
                pitchTypeSelect.prop('selectedIndex',-1);
                pitchTypeSelect.select2_405({
                    'minimumResultsForSearch': '-1'
                });
                pitchTypeSelect.next("span").addClass("pitchReleaseThreeD-shadowed").css("width", "75px");
            });

        pitchTypeSelect
            .change(function() {
                let selectedPitchTypeOption = $("#pitch-type-select option:selected");
                let pitchType = selectedPitchTypeOption.val();
                let byPitchChecked = $('#byPitchToggle').prop("checked");
                let allPitchersChecked = $('#allPitchersToggle').prop("checked");
                let avgChecked = $('#avgToggle').prop("checked");
                if(avgChecked && allPitchersChecked && byPitchChecked){
                    pitchRelData.initiateAvgsAllPitchersPitBrkdwn3D(pitchType);
                } else if(avgChecked && !allPitchersChecked && byPitchChecked){
                    pitchRelData.initiateAvgsOnePitcherPitBrkdwn3D(pitchType);
                } else if(avgChecked && !allPitchersChecked && !byPitchChecked){
                    pitchRelData.initiateAvgsOnePitcherNoPitBrkdwn3D(pitchType);
                } else if(avgChecked && allPitchersChecked && !byPitchChecked){
                    pitchRelData.initiateAvgsAllPitchersNoPitBrkdwn3D(pitchType);
                } else if(!avgChecked){
                    pitchRelData.initiateIndividualSinglePitch3D(pitchType);
                }
            });
        pitchRelData.header.fadeIn();
        $('#allPitchersToggleParent').hide();
        $('#byPitchToggleParent').hide();
        pitchRelData.$loading = $('#loadingDiv').hide();
    },

    initiateIndividualSinglePitch3D: async function(pitchType){
        let individualJsonArr = {}, noPitcher = false, noPitchType = false;
        let jsonComplete = new Promise((resolve) => {
            resolve();
        });
        if((pitchRelData.calculatedMaps.eachPitchForIndividual).hasOwnProperty(pitchRelData.selectedPitcherId)){
            if((pitchRelData.calculatedMaps.eachPitchForIndividual[pitchRelData.selectedPitcherId]).hasOwnProperty(pitchType)){
                individualJsonArr = pitchRelData.calculatedMaps.eachPitchForIndividual[pitchRelData.selectedPitcherId][pitchType];
                noPitcher = false;
                noPitchType = false;
            } else {
                individualJsonArr = pitchRelData.eachPitchForIndividual(pitchType);
                noPitcher = false;
                noPitchType = true;
            }
        } else {
            individualJsonArr = pitchRelData.eachPitchForIndividual(pitchType);
            noPitcher = true;
            noPitchType = true;
        }

        //console.log("totalsJson:");
        //console.log(totalsJson);
        await(jsonComplete);
        if(!noPitcher && noPitchType){
            pitchRelData.calculatedMaps.eachPitchForIndividual[pitchRelData.selectedPitcherId][pitchType] = individualJsonArr;
        } else if(noPitcher && noPitchType){
            pitchRelData.calculatedMaps.eachPitchForIndividual[pitchRelData.selectedPitcherId] = {
                [pitchType]:individualJsonArr
            }
        }
        pitchReleaseThreeD.plotPitches(individualJsonArr, pitchRelData.selectedPitcherId, pitchRelData.isInitial, pitchRelData.avgsSelected, pitchRelData.allPitchersSelected, pitchRelData.pitBrkdwnSelected);
        if(pitchRelData.isInitial){
            pitchRelData.isInitial = false;
        }
    },

    initiateAvgsAllPitchersPitBrkdwn3D: async function(pitchType){
        let totalsJson = {}, noPitchType = false;
        let jsonComplete = new Promise((resolve) => {
            resolve();
        });
        if((pitchRelData.calculatedMaps.totsAvgAllPitchersPitBrkdwn).hasOwnProperty(pitchType)){
            totalsJson = pitchRelData.calculatedMaps.totsAvgAllPitchersPitBrkdwn[pitchType];
            noPitchType = false;
        } else {
            totalsJson = pitchRelData.totsAvgAllPitchersPitBrkdwn(pitchType);
            noPitchType = true;
        }
        //console.log("totalsJson:");
        //console.log(totalsJson);
        await(jsonComplete);
        if(noPitchType) {
            pitchRelData.calculatedMaps.totsAvgAllPitchersPitBrkdwn[pitchType] = totalsJson
        }
        pitchReleaseThreeD.plotPitches(totalsJson, pitchRelData.selectedPitcherId, pitchRelData.isInitial, pitchRelData.avgsSelected, pitchRelData.allPitchersSelected, pitchRelData.pitBrkdwnSelected);
        if(pitchRelData.isInitial){
            pitchRelData.isInitial = false;
        }
    },

    initiateAvgsOnePitcherPitBrkdwn3D: async function(pitchType){
        let totalsJson = {}, noPitcher = false, noPitchType = false;
        let jsonComplete = new Promise((resolve) => {
            resolve();
        });
        if((pitchRelData.calculatedMaps.totsAvgOnePitcherPitBrkdwn).hasOwnProperty(pitchRelData.selectedPitcherId)){
            if((pitchRelData.calculatedMaps.totsAvgOnePitcherPitBrkdwn[pitchRelData.selectedPitcherId]).hasOwnProperty(pitchType)){
                totalsJson = pitchRelData.calculatedMaps.totsAvgOnePitcherPitBrkdwn[pitchRelData.selectedPitcherId][pitchType];
                noPitcher = false;
                noPitchType = false;
            } else {
                totalsJson = pitchRelData.totsAvgOnePitcherPitBrkdwn(pitchType);
                noPitcher = false;
                noPitchType = true;
            }
        } else {
            totalsJson = pitchRelData.totsAvgOnePitcherPitBrkdwn(pitchType);
            noPitcher = true;
            noPitchType = true;
        }

        //console.log("totalsJson:");
        //console.log(totalsJson);
        await(jsonComplete);
        if(!noPitcher && noPitchType){
            pitchRelData.calculatedMaps.totsAvgOnePitcherPitBrkdwn[pitchRelData.selectedPitcherId][pitchType] = totalsJson;
        } else if(noPitcher && noPitchType){
            pitchRelData.calculatedMaps.totsAvgOnePitcherPitBrkdwn[pitchRelData.selectedPitcherId] = {
                [pitchType]:totalsJson
            }
        }
        pitchReleaseThreeD.plotPitches(totalsJson, pitchRelData.selectedPitcherId, pitchRelData.isInitial, pitchRelData.avgsSelected, pitchRelData.allPitchersSelected, pitchRelData.pitBrkdwnSelected);
        if(pitchRelData.isInitial){
            pitchRelData.isInitial = false;
        }
    },

    initiateAvgsAllPitchersNoPitBrkdwn3D: async function(pitchType){
        let totalsJson = {}, noPitchType = false;
        let jsonComplete = new Promise((resolve) => {
            resolve();
        });
        if((pitchRelData.calculatedMaps.totsAvgAllPitchersNoPitBrkdwn).hasOwnProperty(pitchType)){
            totalsJson = pitchRelData.calculatedMaps.totsAvgAllPitchersNoPitBrkdwn[pitchType];
            noPitchType = false;
        } else {
            totalsJson = pitchRelData.totsAvgAllPitchersNoPitBrkdwn(pitchType);
            noPitchType = true;
        }
        //console.log("totalsJson:");
        //console.log(totalsJson);
        await(jsonComplete);
        if(noPitchType) {
            pitchRelData.calculatedMaps.totsAvgAllPitchersNoPitBrkdwn[pitchType] = totalsJson
        }
        pitchReleaseThreeD.plotPitches(totalsJson, pitchRelData.selectedPitcherId, pitchRelData.isInitial, pitchRelData.avgsSelected, pitchRelData.allPitchersSelected, pitchRelData.pitBrkdwnSelected);
        if(pitchRelData.isInitial){
            pitchRelData.isInitial = false;
        }
    },

    initiateAvgsOnePitcherNoPitBrkdwn3D: async function(pitchType){
        let totalsJson = {}, noPitcher = false, noPitchType = false;
        let jsonComplete = new Promise((resolve) => {
            resolve();
        });
        if((pitchRelData.calculatedMaps.totsAvgOnePitcherNoPitBrkdwn).hasOwnProperty(pitchRelData.selectedPitcherId)){
            if((pitchRelData.calculatedMaps.totsAvgOnePitcherNoPitBrkdwn[pitchRelData.selectedPitcherId]).hasOwnProperty(pitchType)){
                totalsJson = pitchRelData.calculatedMaps.totsAvgOnePitcherNoPitBrkdwn[pitchRelData.selectedPitcherId][pitchType];
                noPitcher = false;
                noPitchType = false;
            } else {
                totalsJson = pitchRelData.totsAvgOnePitcherNoPitBrkdwn(pitchType);
                noPitcher = false;
                noPitchType = true;
            }
        } else {
            totalsJson = pitchRelData.totsAvgOnePitcherNoPitBrkdwn(pitchType);
            noPitcher = true;
            noPitchType = true;
        }

        //console.log("totalsJson:");
        //console.log(totalsJson);
        await(jsonComplete);
        if(!noPitcher && noPitchType){
            pitchRelData.calculatedMaps.totsAvgOnePitcherNoPitBrkdwn[pitchRelData.selectedPitcherId][pitchType] = totalsJson;
        } else if(noPitcher && noPitchType){
            pitchRelData.calculatedMaps.totsAvgOnePitcherNoPitBrkdwn[pitchRelData.selectedPitcherId] = {
                [pitchType]:totalsJson
            }
        }
        pitchReleaseThreeD.plotPitches(totalsJson, pitchRelData.selectedPitcherId, pitchRelData.isInitial, pitchRelData.avgsSelected, pitchRelData.allPitchersSelected, pitchRelData.pitBrkdwnSelected);
        if(pitchRelData.isInitial){
            pitchRelData.isInitial = false;
        }
    },



    eachPitchForIndividual: function(pitchTypeSelected){
        let pitcherValues = [];
        for(let i=0;i<pitchRelData.allPitches.length;i++){
            let pitchObj = pitchRelData.allPitches[i];
            let pitchType = pitchObj['piPitchType'];
            let pitcherId = pitchObj['pitcherid'];
            if(pitcherId == pitchRelData.selectedPitcherId) {
                if (pitchType != 'NULL' && pitchType !== null && pitchType!== "XX") {
                    if (pitchType == pitchTypeSelected || pitchTypeSelected === "ALL") {
                        let tempObj = {
                            'relSide': Number(pitchObj['piRelSideCorr']),
                            'relHgt': Number(pitchObj['piRelHeightCorr']),
                            'ext': Number(pitchObj['piExtensionCorr']),
                            'pitchType': pitchType
                        };
                        pitcherValues.push(tempObj);
                    }
                }
            }
        }
        return pitcherValues;
    },

    totsAvgAllPitchersNoPitBrkdwn: function(pitchTypeSelected){
        let pitchersValues = {};
        for(let i=0;i<pitchRelData.allPitches.length;i++){
            let pitchObj = pitchRelData.allPitches[i];
            let pitchType = pitchObj['piPitchType'];
            if(pitchType != 'NULL' && pitchType !== null && pitchType!== "XX") {
                if(pitchType == pitchTypeSelected || pitchTypeSelected === "ALL"){
                let pitcherId = pitchObj['pitcherid'];
                    if (pitchersValues.hasOwnProperty(pitcherId)) {
                        let pitchCount = pitchersValues[pitcherId]['count'];
                        pitchersValues[pitcherId]['count'] = pitchCount + 1;
                        let tempRelSide = Number(pitchObj['piRelSideCorr']);
                        let tempRelSideTot = pitchersValues[pitcherId]['relSideTot'];
                        pitchersValues[pitcherId]['relSideTot'] = tempRelSideTot + tempRelSide;
                        let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                        let tempRelHeightTot = pitchersValues[pitcherId]['relHgtTot'];
                        pitchersValues[pitcherId]['relHgtTot'] = tempRelHeightTot + tempRelHeight;
                        let tempExtension = Number(pitchObj['piExtensionCorr']);
                        let tempExtensionTot = pitchersValues[pitcherId]['extTot'];
                        pitchersValues[pitcherId]['extTot'] = tempExtensionTot + tempExtension;
                    } else {
                        let tempRelSide = Number(pitchObj['piRelSideCorr']);
                        let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                        let tempExtension = Number(pitchObj['piExtensionCorr']);
                        let throws = pitchObj['pitcherthrows'];
                        let team = pitchObj['pitcherteam'];
                        let pitcher = pitchObj['pitcher'];
                        pitchersValues[pitcherId] = {
                            'pitchType':pitchTypeSelected,
                            'count': 1,
                            'relSideTot': tempRelSide,
                            'relHgtTot': tempRelHeight,
                            'extTot': tempExtension,
                            'team': team,
                            'throws': throws,
                            'pitcher': pitcher

                        }
                    }
                }
            }
        }
        return pitchersValues;
    },

    totsAvgAllPitchersPitBrkdwn: function(pitchTypeSelected){
        let pitchersValues = {};
        for(let i=0;i<pitchRelData.allPitches.length;i++){
            let pitchObj = pitchRelData.allPitches[i];
            let pitchType = pitchObj['piPitchType'];
            if(pitchType != 'NULL' && pitchType !== null && pitchType!== "XX") {
                if (pitchType == pitchTypeSelected || pitchTypeSelected === "ALL") {
                    let pitcherId = pitchObj['pitcherid'];
                    if (pitchersValues.hasOwnProperty(pitcherId)) {
                        //console.log(pitchObj['pitcher']);
                        //console.log(pitchObj['pitcherid']);
                        if (pitchersValues[pitcherId].hasOwnProperty(pitchType)) {
                            let pitchCount = pitchersValues[pitcherId][pitchType]['count'];
                            pitchersValues[pitcherId][pitchType]['count'] = pitchCount + 1;
                            let tempRelSide = Number(pitchObj['piRelSideCorr']);
                            let tempRelSideTot = pitchersValues[pitcherId][pitchType]['relSideTot'];
                            pitchersValues[pitcherId][pitchType]['relSideTot'] = tempRelSideTot + tempRelSide;
                            let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                            let tempRelHeightTot = pitchersValues[pitcherId][pitchType]['relHgtTot'];
                            pitchersValues[pitcherId][pitchType]['relHgtTot'] = tempRelHeightTot + tempRelHeight;
                            let tempExtension = Number(pitchObj['piExtensionCorr']);
                            let tempExtensionTot = pitchersValues[pitcherId][pitchType]['extTot'];
                            pitchersValues[pitcherId][pitchType]['extTot'] = tempExtensionTot + tempExtension;
                        } else {
                            let tempRelSide = Number(pitchObj['piRelSideCorr']);
                            let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                            let tempExtension = Number(pitchObj['piExtensionCorr']);
                            let throws = pitchObj['pitcherthrows'];
                            let team = pitchObj['pitcherteam'];
                            let pitcher = pitchObj['pitcher'];
                            pitchersValues[pitcherId][pitchType] = {
                                'count': 1,
                                'relSideTot': tempRelSide,
                                'relHgtTot': tempRelHeight,
                                'extTot': tempExtension,
                                'team': team,
                                'throws': throws,
                                'pitcher': pitcher
                            }
                        }
                    } else {
                        let tempRelSide = Number(pitchObj['piRelSideCorr']);
                        let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                        let tempExtension = Number(pitchObj['piExtensionCorr']);
                        let throws = pitchObj['pitcherthrows'];
                        let team = pitchObj['pitcherteam'];
                        let pitcher = pitchObj['pitcher'];
                        pitchersValues[pitcherId] = {};
                        pitchersValues[pitcherId][pitchType] = {
                            'count': 1,
                            'relSideTot': tempRelSide,
                            'relHgtTot': tempRelHeight,
                            'extTot': tempExtension,
                            'team': team,
                            'throws': throws,
                            'pitcher': pitcher
                        };
                    }
                }
            }
        }
        //console.log("pitchersValues");
        //console.log(pitchersValues);
        return pitchersValues;
    },

    totsAvgOnePitcherPitBrkdwn: function(pitchTypeSelected){
        let pitchersValues = {};
        for(let i=0;i<pitchRelData.allPitches.length;i++){
            let pitchObj = pitchRelData.allPitches[i];
            let pitchType = pitchObj['piPitchType'];
            if(pitchType != 'NULL' && pitchType !== null && pitchType!== "XX") {
                if (pitchType == pitchTypeSelected || pitchTypeSelected === "ALL") {
                    let pitcherId = pitchObj['pitcherid'];
                    if (pitcherId == pitchRelData.selectedPitcherId) {
                        if (pitchersValues.hasOwnProperty(pitcherId)) {
                            if (pitchersValues[pitcherId].hasOwnProperty(pitchType)) {
                                let pitchCount = pitchersValues[pitcherId][pitchType]['count'];
                                pitchersValues[pitcherId][pitchType]['count'] = pitchCount + 1;
                                let tempRelSide = Number(pitchObj['piRelSideCorr']);
                                let tempRelSideTot = pitchersValues[pitcherId][pitchType]['relSideTot'];
                                pitchersValues[pitcherId][pitchType]['relSideTot'] = tempRelSideTot + tempRelSide;
                                let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                                let tempRelHeightTot = pitchersValues[pitcherId][pitchType]['relHgtTot'];
                                pitchersValues[pitcherId][pitchType]['relHgtTot'] = tempRelHeightTot + tempRelHeight;
                                let tempExtension = Number(pitchObj['piExtensionCorr']);
                                let tempExtensionTot = pitchersValues[pitcherId][pitchType]['extTot'];
                                pitchersValues[pitcherId][pitchType]['extTot'] = tempExtensionTot + tempExtension;
                            } else {
                                let tempRelSide = Number(pitchObj['piRelSideCorr']);
                                let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                                let tempExtension = Number(pitchObj['piExtensionCorr']);
                                let throws = pitchObj['pitcherthrows'];
                                let team = pitchObj['pitcherteam'];
                                let pitcher = pitchObj['pitcher'];
                                pitchersValues[pitcherId][pitchType] = {
                                    'count': 1,
                                    'relSideTot': tempRelSide,
                                    'relHgtTot': tempRelHeight,
                                    'extTot': tempExtension,
                                    'team': team,
                                    'throws': throws,
                                    'pitcher': pitcher
                                }
                            }
                        } else {
                            let tempRelSide = Number(pitchObj['piRelSideCorr']);
                            let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                            let tempExtension = Number(pitchObj['piExtensionCorr']);
                            let throws = pitchObj['pitcherthrows'];
                            let team = pitchObj['pitcherteam'];
                            let pitcher = pitchObj['pitcher'];
                            pitchersValues = {};
                            pitchersValues[pitcherId] = {};
                            pitchersValues[pitcherId][pitchType] = {
                                'count': 1,
                                'relSideTot': tempRelSide,
                                'relHgtTot': tempRelHeight,
                                'extTot': tempExtension,
                                'team': team,
                                'throws': throws,
                                'pitcher': pitcher
                            };
                        }
                    }
                }
            }
        }
        return pitchersValues;
    },

    totsAvgOnePitcherNoPitBrkdwn: function(pitchTypeSelected){
        let pitchersValues = {};
        for(let i=0;i<pitchRelData.allPitches.length;i++){
            let pitchObj = pitchRelData.allPitches[i];
            let pitchType = pitchObj['piPitchType'];
            if(pitchType != 'NULL' && pitchType !== null && pitchType!== "XX") {
                let pitcherId = pitchObj['pitcherid'];
                if(pitcherId == pitchRelData.selectedPitcherId) {
                    if (pitchType == pitchTypeSelected || pitchTypeSelected === "ALL") {
                        if (pitchersValues.hasOwnProperty(pitcherId)) {
                            let pitchCount = pitchersValues[pitcherId]['count'];
                            pitchersValues[pitcherId]['count'] = pitchCount + 1;
                            let tempRelSide = Number(pitchObj['piRelSideCorr']);
                            let tempRelSideTot = pitchersValues[pitcherId]['relSideTot'];
                            pitchersValues[pitcherId]['relSideTot'] = tempRelSideTot + tempRelSide;
                            let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                            let tempRelHeightTot = pitchersValues[pitcherId]['relHgtTot'];
                            pitchersValues[pitcherId]['relHgtTot'] = tempRelHeightTot + tempRelHeight;
                            let tempExtension = Number(pitchObj['piExtensionCorr']);
                            let tempExtensionTot = pitchersValues[pitcherId]['extTot'];
                            pitchersValues[pitcherId]['extTot'] = tempExtensionTot + tempExtension;
                        } else {
                            let tempRelSide = Number(pitchObj['piRelSideCorr']);
                            let tempRelHeight = Number(pitchObj['piRelHeightCorr']);
                            let tempExtension = Number(pitchObj['piExtensionCorr']);
                            let throws = pitchObj['pitcherthrows'];
                            let team = pitchObj['pitcherteam'];
                            let pitcher = pitchObj['pitcher'];
                            pitchersValues[pitcherId] = {
                                'pitchType':pitchTypeSelected,
                                'count': 1,
                                'relSideTot': tempRelSide,
                                'relHgtTot': tempRelHeight,
                                'extTot': tempExtension,
                                'team': team,
                                'throws': throws,
                                'pitcher': pitcher

                            }
                        }
                    }
                }
            }
        }
        return pitchersValues;
    },

    populateVariables: async function(){
        //console.log(jsonArr);
        let populate_promise = new Promise((resolve, reject) => {
            resolve();
        });
        //console.log(jsonArr[0]);
        for(let i=0;i<pitchRelData.allPitches.length;i++) {
            let pitchObj = pitchRelData.allPitches[i];
            let pitcherId = pitchObj['pitcherid'];
            let pitcher = pitchObj['pitcher'];
            let pitchType = pitchObj['piPitchType'];
            let team = pitchObj['pitcherteam'];
            if(pitchType != 'NULL' && pitchType !== null && pitchType!== "XX"){
                if(!pitchRelData.pitchersAvailable.hasOwnProperty(pitcherId)){
                    pitchRelData.pitchersAvailable[pitcherId] = pitcher;
                    pitchRelData.pitcherTeams[pitcherId] = team;
                }
                if(!pitchRelData.pitchersPerTeam.hasOwnProperty(team)){
                    let pitcherIdsArr = [];
                    pitcherIdsArr.push(pitcherId);
                    pitchRelData.pitchersPerTeam[team] = pitcherIdsArr;
                    pitchRelData.allTeams.push(team);
                } else {
                    let pitcherIdsArr = pitchRelData.pitchersPerTeam[team];
                    if(!pitchRelData.presentInArray(pitcherIdsArr, pitcherId)) {
                        pitcherIdsArr.push(pitcherId);
                        pitchRelData.pitchersPerTeam[team] = pitcherIdsArr;
                    }
                }
                if(pitchRelData.pitchTypesPerPitcher.hasOwnProperty(pitcherId)){
                    let tempObj = pitchRelData.pitchTypesPerPitcher[pitcherId];
                    if(!tempObj.hasOwnProperty(pitchType)){
                        pitchRelData.pitchTypesPerPitcher[pitcherId][pitchType] = pitchType;
                    }
                } else {
                    pitchRelData.pitchTypesPerPitcher[pitcherId] = {
                        [pitchType]: pitchType
                    };
                }
            }
        }
        await(populate_promise);
        //console.log(pitchRelData.allTeams);
        pitchRelData.initSecondPhase();
        //console.log(pitchRelData.allTeams);
    },

    presentInArray: function(testArr, testVal){
        let newArr = jQuery.grep(testArr, function(n) {
            return n === testVal;
        });

        return (newArr.length > 0);
    },

    getStoredJSON: function(dateBegin, dateEnd, league){
        let datesLeagueKey = dateBegin + "_" + dateEnd + "_" + league;
        let storedObj = sessionStorage.getItem(pitchRelData.ascii_to_hexa(datesLeagueKey));
        let isJson = pitchRelData.isJson(storedObj);
        if(isJson){
            let json = JSON.parse(storedObj);
            let isArray = pitchRelData.isArray(json);
            if(isArray){
                pitchRelData.allPitches = json;
                pitchRelData.populateVariables();
            } else {
                pitchRelData.getJSON(dateBegin, dateEnd, league, datesLeagueKey);
            }
        } else {
            pitchRelData.getJSON(dateBegin, dateEnd, league, datesLeagueKey);
        }
    },

    processJsonFromMongo: async function(data, datesLeagueKey){
        let json = {}, jsonString = "";
        try{
            let json_promise = new Promise((resolve, reject) => {
                resolve();
            });
            json = JSON.parse(data);
            await(json_promise);
            if (json.length !== 0 && json !== undefined) {
                pitchRelData.allPitches = json;
                sessionStorage.setItem(pitchRelData.ascii_to_hexa(datesLeagueKey), JSON.stringify(json));
                pitchRelData.populateVariables();
            } else {
                pitchRelData.clearDatesLevel();
                pitchRelData.newDialogMsg("no pitches for date range and level selected: json");
            }
        } catch (e) {
            try {
                let jsonString_promise = new Promise((resolve, reject) => {
                    resolve();
                });
                jsonString = JSON.stringify(data);
                await(jsonString_promise);
                let json_promise = new Promise((resolve, reject) => {
                    resolve();
                });
                json = JSON.parse(jsonString);
                await(json_promise);
                if (jsonString !== null && json.length !== 0 && json !== undefined) {
                    pitchRelData.allPitches = json;
                    sessionStorage.setItem(pitchRelData.ascii_to_hexa(datesLeagueKey), JSON.stringify(json));
                    pitchRelData.populateVariables();
                } else {
                    pitchRelData.clearDatesLevel();
                    console.log(jsonString);
                    console.log(json);
                    pitchRelData.newDialogMsg("no pitches for date range and level selected: jsonString");
                }
            } catch (e) {
                pitchRelData.clearDatesLevel();
                console.log(json);
                pitchRelData.newDialogMsg("error processing retrieved pitch data");
            }
        }
    },

    processJsonFromSource: async function(data, datesLeagueKey){
        try{
            //console.log("data:");
            //console.log(data);
            if (data.length !== 0 && data !== undefined) {
                if (pitchRelData.isArray(data)) {
                    pitchRelData.allPitches = data;
                    sessionStorage.setItem(pitchRelData.ascii_to_hexa(datesLeagueKey), JSON.stringify(data));
                    pitchRelData.populateVariables();
                } else {
                    pitchRelData.clearDatesLevel();
                    pitchRelData.newDialogMsg("no pitches for date range and level selected: json");
                }
            } else {
                pitchRelData.clearDatesLevel();
                pitchRelData.newDialogMsg("no pitches for date range and level selected: json");
            }
        } catch (e) {
                console.log("error:");
                console.log(e);
                pitchRelData.clearDatesLevel();
                pitchRelData.newDialogMsg("error processing retrieved pitch data");
            }
    },

    getJSON: function(dateBegin, dateEnd, league, datesLeagueKey){
        let days = pitchRelData.datediff(pitchRelData.parseDate(dateEnd), pitchRelData.parseDate(dateBegin));
        console.log("days:");
        console.log(days);
        let extraTime = 0;
        if(days > 2){
            extraTime = Number(days * 1000);
        }
        let totalTime = Number(4000 + extraTime);
        console.log("totalTime: " + totalTime);
        // external mongo source
        //let getURL = "http://localhost:3000/mongo/json?date_begin=" + dateBegin + "&date_end=" + dateEnd + "&level=" + league;
        // local js based source
        //let getURL = "http://localhost:3000/json";
        // internal pi scrubbed source
        let getURL = "http://localhost:8080/test/pitch_release?date_begin=" + dateBegin + "&date_end=" + dateEnd + "&level=" + league;
        $.ajax({
            url: getURL,
            crossDomain: true,
            method: 'GET',
            beforeSend: function(xhr){
                pitchRelData.timeout = setTimeout(function(){
                    pitchRelData.processing = false;
                    xhr.abort();
                    pitchRelData.clearDatesLevel();
                    pitchRelData.newDialogMsg("Request to obtain pitches timed out");
                }, totalTime);
            },
            success: function (data) {
                clearTimeout(pitchRelData.timeout);
                //pitchRelData.processJsonFromMongo(data, datesLeagueKey);
                pitchRelData.processJsonFromSource(data, datesLeagueKey);
            },
            error: function (xhr) {
                clearTimeout(pitchRelData.timeout);
                //console.log(xhr.status);
                pitchRelData.clearDatesLevel();
                pitchRelData.newDialogMsg(xhr.status + ": Error retrieving pitches");
            }
        })
    },

    newDialogMsg: function(message){
        $('#messageDialog p').remove();
        $('#messageDialog').dialog({
            modal:true,
            title:'Error',
            width:400,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            }
        }).append(
            $('<p>').append(message)
        );
    },

    clearDatesLevel: function(){
        $('#header').remove();
        pitchRelData.endDate = "";
        pitchRelData.startDate = "";
        pitchRelData.league = "";
        pitchRelData.initFirstPhase();
    }
};