const MongoClient = require('mongodb').MongoClient;

exports.test = (function (req, res) {
    let beginDate = req.query.date_begin;
    let beginDateParts =beginDate.split('-');
    let endDate = req.query.date_end;
    let endDateParts =endDate.split('-');
    let level = req.query.level;
    let isoBeginDate = beginDateParts[2] + "-" + beginDateParts[0] + "-" + beginDateParts[1] + "T00:00:00Z";
    let isoEndDate = endDateParts[2] + "-" + endDateParts[0] + "-" + endDateParts[1] + "T23:59:59Z";
    console.log("Retreiving from baseball_main mongodb on ds259001.mlab.com:59001 for beginDate: " + isoBeginDate + ", endDate: " + isoEndDate + " and level: " + level);
    MongoClient.connect("mongodb://test_user:password2019@ds259001.mlab.com:59001/baseball_main?maxPoolSize=2&socketTimeoutMS=8000", { useNewUrlParser: true }, function(err, client) {
        if (err) {
            console.error("An error occurred connecting to mlab mogodb on ds259001.mlab.com:59001: ", err);
        } else {
            let db = client.db('baseball_main');
            let collection = db.collection('game_pitches_3');

            collection.aggregate([{$match:{TM_DATE: {$gte: new Date(isoBeginDate), $lte: new Date(isoEndDate)},LEVEL: level}},
                {$project:{ pitcher: { $concat: [ "$PITCHER_LASTNAME", ", ", "$PITCHER_FIRSTNAME" ] },pitcherid: "$PITCHERID", pitcherthrows: "$PITCHERTHROWS", pitcherteam: "$PITCHERTEAM",
                        piPitchType: "$PI_PITCH_TYPE", piRelHeightCorr: "$PI_REL_HEIGHT_CORR", piRelSideCorr: "$PI_REL_SIDE_CORR", piExtensionCorr: "$PI_EXTENSION_CORR", _id: 0}}])
                .toArray(function (err, docs) {
                    //console.log(docs);
                    if (err) {
                        console.error("An error occurred retrieving records for selected date range and level: " + err);
                    }
                    res.json(docs);
                })
        }
    });
});