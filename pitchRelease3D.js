let pitchReleaseThreeD = {

    controlsTarget: [],
    animationFrame: {},
    scene: {},
    renderer:{},
    camera: {},
    controls: {},
    pitchesGroup: new THREE.Group(),
    gridGroup: new THREE.Group(),
    gridStorage: {},
    fieldGroup: {},
    container: {},
    guidMap: {},
    raycaster: new THREE.Raycaster(),
    mouse: {},
    yPos: 0,
    clippingHelpers: new THREE.Group(),
    initialClean: true,
    animating: false,
    rect:{},
    objects:{},
    intersects:[],
    timeout:undefined,
    isCleaning:false,
    moving:false,
    clipPlanes: [
        new THREE.Plane(new THREE.Vector3(0, 0, 1), 0),
        new THREE.Plane(new THREE.Vector3(1, 1, 0), 0),
        new THREE.Plane(new THREE.Vector3(-1, 1, 0), 0)
    ],
    arrowKeysMap: {
        '37':'left_arrow',
        '38':'up_arrow',
        '39':'right_arrow',
        '40':'down_arrow'
    },
    pitchColorMap: {
        "FA": "#ff0000",
        "FT": "#ff0000",
        "SI": "#ffbe00",
        "CH": "#0096ff",
        "FS": "#0096ff", // splitter??
        "FC": "#008000",
        "CB": "#00008b",
        "CU": "#00008b",
        "SL": "#ff7619",
        "KC": "#9400d3",//check on Knuckle-Curve color assignment
        "KN": "#9400d3",
        "EP": "#d3d3d3",//check on Eephus color assignment
        "SC": "#d3d3d3",//check on Screwball color assignment
        "FO": "#d3d3d3",//check on Pitch Out color assignment
        "UN": "#d3d3d3",
        "XX": "#d3d3d3",
        "ALL": "#0000ff",
        null: "#d3d3d3"
    },

    guid: function() {
        return pitchReleaseThreeD.s4() + pitchReleaseThreeD.s4() + '-' + pitchReleaseThreeD.s4() + '-' + pitchReleaseThreeD.s4() + '-' +
            pitchReleaseThreeD.s4() + '-' + pitchReleaseThreeD.s4() + pitchReleaseThreeD.s4() + pitchReleaseThreeD.s4();
    },

    s4: function() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    },

    processPitches: async function(pitchesJson, selectedId, isInitial, isAverage, allPitchers, pitBreakdown) {
        //console.log("pitchesJson");
        //console.log(pitchesJson);
        $('#pitchesData').text("Ext(y) from MP front to BOHP, Hgt(z) from ground, Side(x) from BOHP center (left=neg/right=pos by perspective)");
        let foundTarget = false;
        let vec, nameDiv, idDiv, throwsDiv, teamDiv, pitchTypeDiv, relHeightDiv, relSideDiv, extensionDiv, colorTarget = new THREE.Color('#39ff14'), color;
        let ballGeometry = new THREE.SphereGeometry(0.12, 64, 64);

        let promiseGuidRemove = new Promise((resolve) => {
            resolve();
        });
        if(!isInitial){
            pitchReleaseThreeD.disposePitches();
        }
        await promiseGuidRemove;
        //console.log("pitchReleaseThreeD.pitchesGroup");
        //console.log(pitchReleaseThreeD.pitchesGroup);
        if(isAverage && !pitBreakdown) {
            $.each(pitchesJson, function (key, value) {

                //console.log(key + ": ");
                //console.log(value);
                let pitchType = value['pitchType'];
                let count = Number(value['count']);
                let relSideTot = Number(value['relSideTot']);
                let relHeightTot = Number(value['relHgtTot']);
                let extensionTot = Number(value['extTot']);
                let relSideAvg = Number(relSideTot / count);
                let relHeightAvg = Number(relHeightTot / count);
                let extensionAvg = Number(extensionTot / count);
                //console.log(value['pitcher']);
                //console.log("count: " + value['count']);
                //console.log("extensionSum: " + value['extTot']);
                let x = Number(-1 * relSideAvg);
                let y = Number(59.083333 - extensionAvg);
                let z = Number(relHeightAvg);
                let x_target = Number(-1 * relSideAvg);
                let y_target = Number(50 - extensionAvg);
                let z_target = Number(relHeightAvg);
                color = pitchReleaseThreeD.pitchColorMap[pitchType];
                if (key == selectedId && allPitchers) {
                    foundTarget = true;
                    color = colorTarget;
                    pitchReleaseThreeD.controlsTarget[0] = x_target;
                    pitchReleaseThreeD.controlsTarget[1] = y_target;
                    pitchReleaseThreeD.controlsTarget[2] = z_target;
                }
                // ball release point.
                // For the extension (y), 59.083333 is the position of mound plate front, relative to the back of home plate (as y = 0)
                vec = new THREE.Vector3(x, y, z);

                let ballReleaseMaterial = new THREE.MeshLambertMaterial({
                    color: color,
                    side: THREE.DoubleSide,
                    opacity: 1.0,
                    transparent: true
                });

                let ballRelease = new THREE.Mesh(ballGeometry, ballReleaseMaterial);

                ballRelease.guid = pitchReleaseThreeD.guid();
                ballRelease.player_name = value['pitcher'];
                ballRelease.player_id = key;
                ballRelease.throws = value['throws'];
                ballRelease.team = value['team'];
                ballRelease.rel_height = relHeightAvg.toFixed(3);
                ballRelease.rel_side = relSideAvg.toFixed(3);
                ballRelease.extension = extensionAvg.toFixed(3);
                ballRelease.callback = function () {
                    nameDiv = $('#nameData');
                    nameDiv.text(this.player_name);
                    idDiv = $('#idData');
                    idDiv.text(this.player_id);
                    teamDiv = $('#teamData');
                    teamDiv.text(this.team);
                    throwsDiv = $('#throwsData');
                    throwsDiv.text(this.throws);
                    relHeightDiv = $('#relHeightData');
                    relHeightDiv.text(this.rel_height + "ft");
                    relSideDiv = $('#relSideData');
                    relSideDiv.text(this.rel_side + "ft");
                    extensionDiv = $('#extensionData');
                    extensionDiv.text(this.extension + "ft");
                };

                ballRelease.position.copy(vec);
                pitchReleaseThreeD.pitchesGroup.add(ballRelease)
            });
            if (!foundTarget) {
                pitchReleaseThreeD.controlsTarget[0] = 0;
                pitchReleaseThreeD.controlsTarget[1] = 55.5;
                pitchReleaseThreeD.controlsTarget[2] = 5.5;
            }
            $('#relHeightLabel').text('Avg Rel Height: ');
            $('#relSideLabel').text('Avg Rel Side: ');
            $('#extensionLabel').text('Avg Extension: ');
            $('#pitchInfoHeading').text('Pitcher Info');
            $('.individuals').fadeOut();
            $('.averages').fadeIn();
        } else if(isAverage && pitBreakdown) {
            $.each(pitchesJson, function (topKey, topValue) {
                $.each(topValue, function (key, value) {
                    //console.log(key + ": ");
                    //console.log(value);
                    let count = Number(value['count']);
                    let relSideTot = Number(value['relSideTot']);
                    let relHeightTot = Number(value['relHgtTot']);
                    let extensionTot = Number(value['extTot']);
                    let relSideAvg = Number(relSideTot / count);
                    let relHeightAvg = Number(relHeightTot / count);
                    let extensionAvg = Number(extensionTot / count);
                    //console.log(value['pitcher']);
                    //console.log("count: " + value['count']);
                    //console.log("extensionSum: " + value['extTot']);
                    let x = Number(-1 * relSideAvg);
                    let y = Number(59.083333 - extensionAvg);
                    let z = Number(relHeightAvg);
                    let x_target = Number(-1 * relSideAvg);
                    let y_target = Number(50 - extensionAvg);
                    let z_target = Number(relHeightAvg);
                    color = pitchReleaseThreeD.pitchColorMap[key];
                    if (topKey == selectedId && allPitchers) {
                        foundTarget = true;
                        color = colorTarget;
                        pitchReleaseThreeD.controlsTarget[0] = x_target;
                        pitchReleaseThreeD.controlsTarget[1] = y_target;
                        pitchReleaseThreeD.controlsTarget[2] = z_target;
                    }
                    // ball release point.
                    // For the extension (y), 59.083333 is the position of mound plate front, relative to the back of home plate (as y = 0)
                    vec = new THREE.Vector3(x, y, z);

                    let ballReleaseMaterial = new THREE.MeshLambertMaterial({
                        color: color,
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true
                    });

                    let ballRelease = new THREE.Mesh(ballGeometry, ballReleaseMaterial);

                    ballRelease.guid = pitchReleaseThreeD.guid();
                    ballRelease.player_name = value['pitcher'];
                    ballRelease.player_id = topKey;
                    ballRelease.throws = value['throws'];
                    ballRelease.team = value['team'];
                    ballRelease.rel_height = relHeightAvg.toFixed(3);
                    ballRelease.rel_side = relSideAvg.toFixed(3);
                    ballRelease.extension = extensionAvg.toFixed(3);
                    ballRelease.callback = function () {
                        pitchTypeDiv = $('#pitchTypeData');
                        pitchTypeDiv.text(key);
                        nameDiv = $('#nameData');
                        nameDiv.text(this.player_name);
                        idDiv = $('#idData');
                        idDiv.text(this.player_id);
                        teamDiv = $('#teamData');
                        teamDiv.text(this.team);
                        throwsDiv = $('#throwsData');
                        throwsDiv.text(this.throws);
                        relHeightDiv = $('#relHeightData');
                        relHeightDiv.text(this.rel_height + "ft");
                        relSideDiv = $('#relSideData');
                        relSideDiv.text(this.rel_side + "ft");
                        extensionDiv = $('#extensionData');
                        extensionDiv.text(this.extension + "ft");
                    };

                    ballRelease.position.copy(vec);
                    pitchReleaseThreeD.pitchesGroup.add(ballRelease)
                });
            });
            if (!foundTarget) {
                pitchReleaseThreeD.controlsTarget[0] = 0;
                pitchReleaseThreeD.controlsTarget[1] = 55.5;
                pitchReleaseThreeD.controlsTarget[2] = 5.5;
            }
            $('#relHeightLabel').text('Avg Rel Height: ');
            $('#relSideLabel').text('Avg Rel Side: ');
            $('#extensionLabel').text('Avg Extension: ');
            $('#pitchInfoHeading').text('Pitcher Info');
            $('.individuals').fadeOut();
            $('.averages').fadeIn();
            $('#pitchTypeParent').fadeIn();
        } else {
            let length = pitchesJson.length;
            for (let i=0;i<length;i++){
                let pitchObj = pitchesJson[i];
                let relSide = Number(pitchObj['relSide']);
                let relHeight = Number(pitchObj['relHgt']);
                let extension = Number(pitchObj['ext']);
                let x = Number(-1 * relSide);
                let y = Number(59.083333 - extension);
                let z = Number(relHeight);
                color = pitchReleaseThreeD.pitchColorMap[pitchObj['pitchType']];

                vec = new THREE.Vector3(x, y, z);

                let ballReleaseMaterial = new THREE.MeshLambertMaterial({
                    color: color,
                    side: THREE.DoubleSide,
                    opacity: 1.0,
                    transparent: true
                });

                let ballRelease = new THREE.Mesh(ballGeometry, ballReleaseMaterial);

                ballRelease.guid = pitchReleaseThreeD.guid();
                ballRelease.pitch_type = pitchObj['pitchType'];
                ballRelease.rel_height = relHeight.toFixed(3);
                ballRelease.rel_side = relSide.toFixed(3);
                ballRelease.extension = extension.toFixed(3);
                ballRelease.callback = function () {
                    pitchTypeDiv = $('#pitchTypeData');
                    pitchTypeDiv.text(this.pitch_type);
                    relHeightDiv = $('#relHeightData');
                    relHeightDiv.text(this.rel_height + "ft");
                    relSideDiv = $('#relSideData');
                    relSideDiv.text(this.rel_side + "ft");
                    extensionDiv = $('#extensionData');
                    extensionDiv.text(this.extension + "ft");
                };

                ballRelease.position.copy(vec);
                pitchReleaseThreeD.pitchesGroup.add(ballRelease)
            }
            pitchReleaseThreeD.controlsTarget[0] = 0;
            pitchReleaseThreeD.controlsTarget[1] = 55.5;
            pitchReleaseThreeD.controlsTarget[2] = 5.5;
            $('#relHeightLabel').text('Rel Height: ');
            $('#relSideLabel').text('Rel Side: ');
            $('#extensionLabel').text('Extension: ');
            $('#pitchInfoHeading').text('Pitch Info');
            $('.averages').fadeOut();
            $('.individuals').fadeIn();
        }
        if(isInitial){
            pitchReleaseThreeD.scene.add(pitchReleaseThreeD.pitchesGroup);
        }

        pitchReleaseThreeD.setGuids(isInitial, pitchesJson);
    },

    setGuids: function(isInitial, totalsJson){
        let objects = pitchReleaseThreeD.pitchesGroup.children;
        let length = objects.length;
        for (let i = 0; i < length; i++) {
            let guid = objects[i].guid;
            pitchReleaseThreeD.guidMap[guid] = objects[i]
        }
        if(isInitial){
            pitchReleaseThreeD.finalize();
        } else {
            pitchReleaseThreeD.camera.updateMatrixWorld();
            pitchReleaseThreeD.controls.update();
            totalsJson = undefined;
            if(!pitchReleaseThreeD.animating) {
                pitchReleaseThreeD.animating = true;
                pitchReleaseThreeD.startAnimation();
            }
            if (pitchReleaseThreeD.timeout !== undefined) {
                window.clearTimeout(pitchReleaseThreeD.timeout);
            }
            pitchReleaseThreeD.timeout = window.setTimeout(function () {
                pitchReleaseThreeD.stopAnimation();
            }, 300);
        }
    },

    plotPitches: function(totalsJson, selectedId, isInitial, isAverage, allPitchers, pitBreakdown) {

        let homePlateDim = 1.416666666666667; // Length and width w/o clipped back corners
        let halfHPDim = Number(homePlateDim/2);

        $('#loadingDiv').show();

        if(!isInitial){
            pitchReleaseThreeD.processPitches(totalsJson, selectedId, isInitial, isAverage, allPitchers, pitBreakdown);
        } else {

            let lessHeight = Number(window.innerHeight*.163);
            let newHeight = Number(window.innerHeight - lessHeight);
            let lessWidth = Number(window.innerWidth*.02);
            let newWidth = Number(window.innerWidth - lessWidth);
            //console.log("innerWidth: " + window.innerWidth);
            //console.log("lessWidth: " + lessWidth);
            //console.log("newWidth: " + newWidth);
            // renderer
            pitchReleaseThreeD.renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
            pitchReleaseThreeD.renderer.setSize(newWidth, newHeight);
            pitchReleaseThreeD.renderer.localClippingEnabled = true;
            //renderer.physicallyCorrectLights = true;
            pitchReleaseThreeD.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
            pitchReleaseThreeD.renderer.setPixelRatio(window.devicePixelRatio);
            pitchReleaseThreeD.renderer.setSize(newWidth, newHeight);

            //var nEnd = 0, nMax, nStep = 90;// 30 faces * 3 vertices/face
            pitchReleaseThreeD.pitchesGroup = new THREE.Group();
            pitchReleaseThreeD.mouse = new THREE.Vector2();

            init();
        }

        function init() {

            pitchReleaseThreeD.yPos = $('#header').height();

            pitchReleaseThreeD.container = $('<div id="containerDiv" style="position:relative;display:block;"></div>');
            pitchReleaseThreeD.container.appendTo($('body'));

            // scene
            pitchReleaseThreeD.scene = new THREE.Scene();

            let measurements = $('<div id="measurements" style="position:fixed; left:1px; bottom:2px; margin-left: 8px; margin-bottom: 5px; display:inline-block; max-width:310px; text-align:left; background-color: #d9d9d9; border: 3px solid rgb(40,40,40);" class="ui-corner-all"></div>');
            measurements.hover(function(){
                measurements.css( 'cursor', 'pointer' );
            });
            let gridsButton = $('<button id="gridsButton" class="clicky pitchRelease3D-font" style="display:inline-block">Grids Off</button>');
            let gridsText = $('<div id="gridsText" class="pitchRelease3D-font" style="display:inline-block; margin-left:10px; font-size:18px; font-weight:bolder">Gridlines in 5ft increments</div>');
            let gridsDiv = $('<div id="grids" state="off" style="position:fixed; left:0px; bottom:5px; display:inline-block"></div>');
            gridsDiv.append(gridsButton);
            gridsDiv.append(gridsText);
            let info = $('<div id="pitchInfo" style="position:fixed; bottom:1px; right:1px; min-width:200px; text-align:left; background-color: #d9d9d9; border: 3px solid rgb(40,40,40);" class="ui-corner-all ui-widget"></div>');

            let measureHeading = $('<div class="pitchRelease3D-font" style="font-size:16px; display:block; text-align:left; margin: 2px 5px; border-bottom: 2px ridge rgb(40,40,40);width:110px; font-weight:bold">Measurements</div>');
            let pitchInfoHeading = $('<div id="pitchInfoHeading" class="inherit" style="font-weight:bold; display:block; text-align:left; margin-left:5px; margin-top:2px; margin-bottom:5px; border-bottom: 2px ridge rgb(40,40,40);width:120px">Pitcher Info</div>');

            function buildInfoElement(forAveragesOnly, forIndividualsOnly){
                if(forAveragesOnly){
                    return $('<div class="inherit averages" style="display:block; text-align:left; max-width:300px; margin: 2px 5px 5px;"></div>');
                } else if(forIndividualsOnly){
                    return $('<div class="inherit individuals" style="display:block; text-align:left; max-width:300px; margin: 2px 5px 5px;"></div>');
                } else {
                    return $('<div class="inherit" style="display:block; text-align:left; max-width:300px; margin: 2px 5px 5px;"></div>');
                }

            }

            function buildInfoLabel(text, id) {
                return $('<div id="'+id+'" style="display:inline-block; text-align:left; max-width:300px; margin: 2px 2px 5px;">'+text+'</div>');
            }

            function buildLegendLabel(text) {
                return $('<div class="pitchRelease3D-font" style="display:block; text-align:left; font-size:12px; margin:2px">'+text+'</div>');
            }

            function buildInfoData(id) {
                return $('<div id="'+id+'" style="display:inline-block; text-align:left; max-width:300px; margin: 2px 0;"> </div>');
            }

            function buildMeasureElement(){
                return $('<div class="pitchRelease3D-font" style="display:block; text-align:left; max-width:300px; margin: 0 5px 5px; font-size:10px"></div>');
            }

            function buildMeasureLabel(text){
                return $('<div class="pitchRelease3D-font" style="display:inline-block; text-align:left; margin:2px 5px 2px 0; font-size:12px; font-weight:bold; vertical-align:top">'+text+'</div>');
            }

            function buildMeasureData(id) {
                return $('<div id="'+id+'" class="pitchRelease3D-font" style="display:inline-block; text-align:left; max-width:200px; margin:2px 0 2px 0; font-size:12px"> </div>');
            }

            let legendDiv = buildInfoElement();
            let backOfHomePlateDiv = buildMeasureElement();
            let homePlateDiv = buildMeasureElement();
            let mountPlateDiv = buildMeasureElement();
            let moundDiv = buildMeasureElement();
            let basesDimDiv = buildMeasureElement();
            let basesPosDiv = buildMeasureElement();
            let foulDiv = buildMeasureElement();
            let fieldDiv = buildMeasureElement();
            let pitchesDiv = buildMeasureElement();

            let nameDiv = buildInfoElement(true, false);
            let idDiv = buildInfoElement(true, false);
            let teamDiv = buildInfoElement(true, false);
            let throwsDiv = buildInfoElement(true, false);
            let pitchTypeDiv = buildInfoElement(false, true);
                pitchTypeDiv.attr('id', 'pitchTypeParent');
            let relHeightDiv = buildInfoElement(false, false);
            let relSideDiv = buildInfoElement(false, false);
            let extensionDiv = buildInfoElement(false, false);

            legendDiv
                .append(buildLegendLabel("BOHP = Back of Home Plate @ -17in"))
                .append(buildLegendLabel("HP = Home Plate, MP = Mound Plate (front @60.5ft)"))
                .append(buildLegendLabel("Ext = Extension, Hgt = Height"));
            backOfHomePlateDiv
                .append(buildMeasureLabel("BOHP Pos: "))
                .append(buildMeasureData('backOfHomePlateData'));
            homePlateDiv
                .append(buildMeasureLabel("HP Dim/Pos: "))
                .append(buildMeasureData('homePlateData'));
            mountPlateDiv
                .append(buildMeasureLabel("MP Dim/Pos: "))
                .append(buildMeasureData('moundPlateData'));
            moundDiv
                .append(buildMeasureLabel("Mound Dim/Pos: "))
                .append(buildMeasureData('moundData'));
            basesDimDiv
                .append(buildMeasureLabel("Bases Dim: "))
                .append(buildMeasureData('basesDimData'));
            basesPosDiv
                .append(buildMeasureLabel("Bases Pos: "))
                .append(buildMeasureData('basesPosData'));
            foulDiv
                .append(buildMeasureLabel("Foul Poles Pos: "))
                .append(buildMeasureData('foulPolesData'));
            fieldDiv
                .append(buildMeasureLabel("Center Field Dim: "))
                .append(buildMeasureData('centerFieldData'));
            pitchesDiv
                .append(buildMeasureLabel("Pitch Dim/Pos: "))
                .append(buildMeasureData('pitchesData'));

            nameDiv
                .append(buildInfoLabel("Name: ", 'nameLabel'))
                .append(buildInfoData('nameData'));
            idDiv
                .append(buildInfoLabel("MLBAM ID: ", 'idLabel'))
                .append(buildInfoData('idData'));
            teamDiv
                .append(buildInfoLabel("Team: ", 'teamLabel'))
                .append(buildInfoData('teamData'));
            throwsDiv
                .append(buildInfoLabel("Throws: ", 'throwsLabel'))
                .append(buildInfoData('throwsData'));
            pitchTypeDiv
                .append(buildInfoLabel("Pitch Type: ", 'pitchTypeLabel'))
                .append(buildInfoData('pitchTypeData'));
            relHeightDiv
                .append(buildInfoLabel("Avg Rel Height: ", 'relHeightLabel'))
                .append(buildInfoData('relHeightData'));
            relSideDiv
                .append(buildInfoLabel("Avg Rel Side: ", 'relSideLabel'))
                .append(buildInfoData('relSideData'));
            extensionDiv
                .append(buildInfoLabel("Avg Extension: ", 'extensionLabel'))
                .append(buildInfoData('extensionData'));

            let allMeasure = $('<div id="allMeasurements" state="closed" style="display:block"></div>');

            allMeasure
                .append(legendDiv)
                .append(backOfHomePlateDiv)
                .append(homePlateDiv)
                .append(mountPlateDiv)
                .append(moundDiv)
                .append(basesDimDiv)
                .append(basesPosDiv)
                .append(foulDiv)
                .append(fieldDiv)
                .append(pitchesDiv);

            measurements
                .append(measureHeading)
                .append(allMeasure);

            gridsButton.click(function(){
                gridsText.toggle();
                let state = gridsDiv.attr("state");
                if(state === "off"){
                    pitchReleaseThreeD.reAddGrids();
                    gridsDiv.attr("state", "on");
                    gridsButton.text("Grids On");
                } else if(state === "on"){
                    pitchReleaseThreeD.removeGrids();
                    gridsDiv.attr("state", "off");
                    gridsButton.text("Grids Off");
                }
            });

            measurements.click(function(){
                let state = allMeasure.attr("state");
                //console.log("state:");
                //console.log(state);
                allMeasure.toggle();
                if(state === "open"){
                    allMeasure.attr("state", "closed");
                } else {
                    allMeasure.attr("state", "open");
                }
                let newWidth = measurements.width();
                gridsDiv.css("left", Number(newWidth + 20) + "px");
            });

            info
                .append(pitchInfoHeading)
                .append(nameDiv)
                .append(idDiv)
                .append(teamDiv)
                .append(throwsDiv)
                .append(pitchTypeDiv)
                .append(relHeightDiv)
                .append(relSideDiv)
                .append(extensionDiv);

            pitchReleaseThreeD.container.append(info);
            pitchReleaseThreeD.container.append(measurements);
            pitchReleaseThreeD.container.append(gridsDiv);
            gridsText.toggle();
            allMeasure.toggle();
            let newWidth = measurements.width();
            gridsDiv.css("left", Number(newWidth + 20) + "px");
            pitchReleaseThreeD.container.append(pitchReleaseThreeD.renderer.domElement);

            pitchReleaseThreeD.camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 600);
            pitchReleaseThreeD.camera.position.set(0, 61, 6);
            pitchReleaseThreeD.camera.up.set(0, 0, 1);

            pitchReleaseThreeD.scene.background = new THREE.Color(0xf0f0f0);
            pitchReleaseThreeD.scene.add(pitchReleaseThreeD.camera); //required, since camera has a child light

            // ambient
            pitchReleaseThreeD.scene.add(new THREE.AmbientLight(0x404040, 1));

            // light
            let light = new THREE.PointLight(0xffffff, 1, 0, 2);
            light.position.set(20, 70, 20);
            light.shadow.mapSize.width = 1024; // default is 512
            light.shadow.mapSize.height = 1024; // default is 512
            pitchReleaseThreeD.camera.add(light);

            pitchReleaseThreeD.fieldGroup = new THREE.Object3D();
            pitchReleaseThreeD.mouse = new THREE.Vector2();

            outfieldGrass();
        }

        function homePlate() {
            $('#backOfHomePlateData').text("x = 0.0, y = -1.416667, z = 0");
            $('#homePlateData').text("17\'l x 17\'w, Two 8.5\" x 8.5\" x 12\" triangles cut @ back");
            let batPlateShape = new THREE.Shape();

            batPlateShape.moveTo(-halfHPDim, 0, 0);
            batPlateShape.lineTo(-halfHPDim, -halfHPDim, 0);
            batPlateShape.lineTo(0, -homePlateDim, 0);
            batPlateShape.lineTo(halfHPDim, -halfHPDim, 0);
            batPlateShape.lineTo(halfHPDim, 0, 0);
            batPlateShape.lineTo(-halfHPDim, 0, 0);
            batPlateShape.lineTo(-halfHPDim, -halfHPDim, 0);
            batPlateShape.lineTo(0, -homePlateDim, 0);
            batPlateShape.lineTo(halfHPDim, -halfHPDim, 0);
            batPlateShape.lineTo(halfHPDim, 0, 0);

            let plateExtrusion = {
                steps: 2,
                depth: .1,
                bevelEnabled: false
            };

            let batPlateGeometry = new THREE.ExtrudeGeometry(batPlateShape, plateExtrusion);
            batPlateGeometry.mergeVertices();
            batPlateGeometry.computeVertexNormals(true);
            batPlateGeometry.computeFaceNormals();
            let batPlateMaterial = new THREE.MeshLambertMaterial({
                color: 'white',
                side: THREE.DoubleSide,
                reflectivity:0.1,
                transparent: false
            });
            let batPlateMesh = new THREE.Mesh(batPlateGeometry, batPlateMaterial);

            batPlateMesh.renderOrder = -2;
            batPlateMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(batPlateMesh);

            pitchingMound();
        }

        function batterBoxes() {
            let rhBatterBoxShape = new THREE.Shape();
            let rhBoxRightX = -Number(0.7083333333333333 + 0.5);
            let rhBoxLeftX = -Number(0.7083333333333333 + 4.5);
            let rhBoxTopY = Number(3 - 0.7083333333333333);
            let rhBoxBotY = -Number(3 + 0.7083333333333333);

            let rhInnerBatterBoxShape = new THREE.Shape();
            let rhInnerBoxRightX = -Number(0.7083333333333333 + 0.5 + 0.3333333333333333);
            let rhInnerBoxLeftX = -Number(0.7083333333333333 + 4.5 - 0.3333333333333333);
            let rhInnerBoxTopY = Number(3 - 0.7083333333333333 - 0.3333333333333333);
            let rhInnerBoxBotY = -Number(3 + 0.7083333333333333 - 0.3333333333333333);

            let lhBatterBoxShape = new THREE.Shape();
            let lhBoxRightX = Number(0.7083333333333333 + 0.5);
            let lhBoxLeftX = Number(0.7083333333333333 + 4.5);
            let lhBoxTopY = Number(3 - 0.7083333333333333);
            let lhBoxBotY = -Number(3 + 0.7083333333333333);

            let lhInnerBatterBoxShape = new THREE.Shape();
            let lhInnerBoxLeftX = Number(0.7083333333333333 + 0.5 + 0.3333333333333333);
            let lhInnerBoxRightX = Number(0.7083333333333333 + 4.5 - 0.3333333333333333);
            let lhInnerBoxTopY = Number(3 - 0.7083333333333333 - 0.3333333333333333);
            let lhInnerBoxBotY = -Number(3 + 0.7083333333333333 - 0.3333333333333333);

            rhBatterBoxShape.moveTo(rhBoxRightX, rhBoxTopY, 0);
            rhBatterBoxShape.lineTo(rhBoxLeftX, rhBoxTopY, 0);
            rhBatterBoxShape.lineTo(rhBoxLeftX, rhBoxBotY, 0);
            rhBatterBoxShape.lineTo(rhBoxRightX, rhBoxBotY, 0);
            rhBatterBoxShape.lineTo(rhBoxRightX, rhBoxTopY, 0);

            rhInnerBatterBoxShape.moveTo(rhInnerBoxRightX, rhInnerBoxTopY, 0);
            rhInnerBatterBoxShape.lineTo(rhInnerBoxLeftX, rhInnerBoxTopY, 0);
            rhInnerBatterBoxShape.lineTo(rhInnerBoxLeftX, rhInnerBoxBotY, 0);
            rhInnerBatterBoxShape.lineTo(rhInnerBoxRightX, rhInnerBoxBotY, 0);
            rhInnerBatterBoxShape.lineTo(rhInnerBoxRightX, rhInnerBoxTopY, 0);

            lhBatterBoxShape.moveTo(lhBoxRightX, lhBoxTopY, 0);
            lhBatterBoxShape.lineTo(lhBoxLeftX, lhBoxTopY, 0);
            lhBatterBoxShape.lineTo(lhBoxLeftX, lhBoxBotY, 0);
            lhBatterBoxShape.lineTo(lhBoxRightX, lhBoxBotY, 0);
            lhBatterBoxShape.lineTo(lhBoxRightX, lhBoxTopY, 0);

            lhInnerBatterBoxShape.moveTo(lhInnerBoxRightX, lhInnerBoxTopY, 0);
            lhInnerBatterBoxShape.lineTo(lhInnerBoxLeftX, lhInnerBoxTopY, 0);
            lhInnerBatterBoxShape.lineTo(lhInnerBoxLeftX, lhInnerBoxBotY, 0);
            lhInnerBatterBoxShape.lineTo(lhInnerBoxRightX, lhInnerBoxBotY, 0);
            lhInnerBatterBoxShape.lineTo(lhInnerBoxRightX, lhInnerBoxTopY, 0);

            let rhBatterBoxGeometry = new THREE.ShapeGeometry(rhBatterBoxShape, 24);

            let rhInnerBatterBoxGeometry = new THREE.ShapeGeometry(rhInnerBatterBoxShape, 24);


            let lhBatterBoxGeometry = new THREE.ShapeGeometry(lhBatterBoxShape, 24);


            let lhInnerBatterBoxGeometry = new THREE.ShapeGeometry(lhInnerBatterBoxShape, 24);

            let batterBoxMaterial = new THREE.MeshLambertMaterial({
                color: 'white',
                side: THREE.DoubleSide,
                reflectivity:0.1,
                transparent: false
            });

            let innerBatterBoxMaterial = new THREE.MeshLambertMaterial({
                color: 'rgb(139,69,19)',
                side: THREE.DoubleSide,
                reflectivity:0.1,
                transparent: false
            });

            let rhBatterBoxMesh = new THREE.Mesh(rhBatterBoxGeometry, batterBoxMaterial);
            rhBatterBoxMesh.renderOrder = -3;
            rhBatterBoxMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            let rhInnerBatterBoxMesh = new THREE.Mesh(rhInnerBatterBoxGeometry, innerBatterBoxMaterial);
            rhInnerBatterBoxMesh.renderOrder = -3;
            rhInnerBatterBoxMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            let lhBatterBoxMesh = new THREE.Mesh(lhBatterBoxGeometry, batterBoxMaterial);
            lhBatterBoxMesh.renderOrder = -3;
            lhBatterBoxMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            let lhInnerBatterBoxMesh = new THREE.Mesh(lhInnerBatterBoxGeometry, innerBatterBoxMaterial);
            lhInnerBatterBoxMesh.renderOrder = -3;
            lhInnerBatterBoxMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(rhBatterBoxMesh);
            pitchReleaseThreeD.fieldGroup.add(rhInnerBatterBoxMesh);
            pitchReleaseThreeD.fieldGroup.add(lhBatterBoxMesh);
            pitchReleaseThreeD.fieldGroup.add(lhInnerBatterBoxMesh);

            catcherBox();
        }

        function catcherBox() {

            let catcherBoxShape = new THREE.Shape();

            let catcherBoxTopY = -Number(3 + 0.7083333333333333);

            let catcherBoxRightOutX = Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667);
            let catcherBoxLeftOutX = -Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667);
            let catcherBoxBotOutY = -Number(8 + 0.7083333333333333);

            let catcherBoxRightInX = Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667 - 0.3333333333333333);
            let catcherBoxLeftIntX = -Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667 - 0.3333333333333333);
            let catcherBoxBotInY = -Number(8 + 0.7083333333333333 - 0.3333333333333333);

            catcherBoxShape.moveTo(catcherBoxRightOutX, catcherBoxTopY, 0);
            catcherBoxShape.lineTo(catcherBoxRightOutX, catcherBoxBotOutY, 0);
            catcherBoxShape.lineTo(catcherBoxLeftOutX, catcherBoxBotOutY, 0);
            catcherBoxShape.lineTo(catcherBoxLeftOutX, catcherBoxTopY, 0);
            catcherBoxShape.lineTo(catcherBoxLeftIntX, catcherBoxTopY, 0);
            catcherBoxShape.lineTo(catcherBoxLeftIntX, catcherBoxBotInY, 0);
            catcherBoxShape.lineTo(catcherBoxRightInX, catcherBoxBotInY, 0);
            catcherBoxShape.lineTo(catcherBoxRightInX, catcherBoxTopY, 0);
            catcherBoxShape.lineTo(catcherBoxRightOutX, catcherBoxTopY, 0);

            let catcherBoxGeometry = new THREE.ShapeGeometry(catcherBoxShape, 24);
            catcherBoxGeometry.mergeVertices();
            catcherBoxGeometry.computeVertexNormals(true);
            catcherBoxGeometry.computeFaceNormals();

            let catcherBoxMaterial = new THREE.MeshLambertMaterial({
                color: 'white',
                side: THREE.DoubleSide,
                reflectivity: 0.1,
                transparent: false
            });

            let catcherBoxMesh = new THREE.Mesh(catcherBoxGeometry, catcherBoxMaterial);
            catcherBoxMesh.renderOrder = -3;
            catcherBoxMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(catcherBoxMesh);

            foulLines();
        }

        function foulLines() {
            let leftFieldPoleDist = -230;
            let leftFieldPolePlusFour = Number(leftFieldPoleDist + 0.3333333333333333);
            let rhBoxTopY = Number(3 - 0.7083333333333333);

            let rightFieldPoleDist = 230;
            let rightFieldPolePlusFour = Number(rightFieldPoleDist + 0.3333333333333333);
            let lhBoxTopY = Number(3 - 0.7083333333333333);

            let leftFoulShape = new THREE.Shape();

            let rightFoulShape = new THREE.Shape();

            leftFoulShape.moveTo(-Number(rhBoxTopY + 1.5), rhBoxTopY, 0);
            leftFoulShape.lineTo(-Number(rhBoxTopY + 1.5 - 0.3333333333333333), rhBoxTopY, 0);
            leftFoulShape.lineTo(leftFieldPoleDist, -Number(leftFieldPoleDist + 9.7083333333333333), 0);
            leftFoulShape.lineTo(leftFieldPolePlusFour, -Number(leftFieldPoleDist + 9.7083333333333333 + 0.3333333333333333), 0);
            leftFoulShape.lineTo(-Number(rhBoxTopY + 1.7083333333333333), rhBoxTopY, 0);

            rightFoulShape.moveTo(Number(lhBoxTopY + 1.5), lhBoxTopY, 0);
            rightFoulShape.lineTo(Number(lhBoxTopY + 1.5 - 0.3333333333333333), rhBoxTopY, 0);
            rightFoulShape.lineTo(rightFieldPoleDist, Number(rightFieldPoleDist - 8.7083333333333333), 0);
            rightFoulShape.lineTo(rightFieldPolePlusFour, Number(rightFieldPoleDist - 8.7083333333333333 + 0.3333333333333333), 0);
            rightFoulShape.lineTo(Number(lhBoxTopY + 1.7083333333333333), lhBoxTopY, 0);

            let leftFoulGeometry = new THREE.ShapeGeometry(leftFoulShape, 24);
            leftFoulGeometry.mergeVertices();
            leftFoulGeometry.computeVertexNormals(true);
            leftFoulGeometry.computeFaceNormals();

            let rightFoulGeometry = new THREE.ShapeGeometry(rightFoulShape, 24);
            rightFoulGeometry.mergeVertices();
            rightFoulGeometry.computeVertexNormals(true);
            rightFoulGeometry.computeFaceNormals();

            let foulMaterial = new THREE.MeshLambertMaterial({
                color: 'white',
                side: THREE.DoubleSide,
                reflectivity:0.1,
                transparent: false
            });

            let leftFoulMesh = new THREE.Mesh(leftFoulGeometry, foulMaterial);
            leftFoulMesh.renderOrder = -3;
            leftFoulMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            let rightFoulMesh = new THREE.Mesh(rightFoulGeometry, foulMaterial);
            rightFoulMesh.renderOrder = -3;
            rightFoulMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(leftFoulMesh);

            pitchReleaseThreeD.fieldGroup.add(rightFoulMesh);

            foulPoles();
        }

        function foulPoles() {
            $('#foulPolesData').text('327ft @ 45deg diagonal from BOHP');

            let foulPoleGeometry = new THREE.CylinderGeometry( .5, .5, 40, 32 );
            foulPoleGeometry.mergeVertices();
            foulPoleGeometry.computeVertexNormals(true);
            foulPoleGeometry.computeFaceNormals();
            let foulPoleMaterial = new THREE.MeshLambertMaterial({
                color: 'yellow',
                side: THREE.DoubleSide,
                reflectivity:0.3,
                transparent: false
            });
            let leftFoulPoleCylinder = new THREE.Mesh( foulPoleGeometry, foulPoleMaterial );
            leftFoulPoleCylinder.position.set(-Number(235 - 0.1666666666666667 - 0.5), Number(225.5 - 0.7083333333333333 + 0.1666666666666667), 20);
            leftFoulPoleCylinder.rotation.x = Math.PI / 2;
            let rightFoulPoleCylinder = new THREE.Mesh( foulPoleGeometry, foulPoleMaterial );
            rightFoulPoleCylinder.position.set(Number(235 - 1.1666666666666667), Number(225.5 - 0.7083333333333333 + 0.1666666666666667), 20);
            rightFoulPoleCylinder.rotation.x = Math.PI / 2;
            //let foulPoleHelper = new THREE.Mesh( foulPoleGeometry, foulPoleMaterial );
            //foulPoleHelper.position.set(0, 398.583333, 20);
            //foulPoleHelper.rotation.x = Math.PI / 2;
            pitchReleaseThreeD.fieldGroup.add(leftFoulPoleCylinder);
            pitchReleaseThreeD.fieldGroup.add(rightFoulPoleCylinder);
            //pitchReleaseThreeD.fieldGroup.add(foulPoleHelper);

            homePlate();
        }

        function bases() {

            $('#basesDimData').text("15\"l x 15\"w square");
            $('#basesPosData').text("1st & 3rd @ 90ft diag from BOHP, 2nd @ 127.28ft from BOHP");
            let baseShape = new THREE.Shape();

            baseShape.moveTo(0, 0, 0);
            baseShape.lineTo(1.25, 0, 0);
            baseShape.lineTo(1.25, 1.25, 0);
            baseShape.lineTo(0, 1.25, 0);
            baseShape.lineTo(0, 0, 0);

            let baseExtrusion = {
                steps: 2,
                depth: .2,
                bevelEnabled: false
            };

            let baseGeometry = new THREE.ExtrudeGeometry(baseShape, baseExtrusion);
            baseGeometry.mergeVertices();
            baseGeometry.computeVertexNormals(true);
            baseGeometry.computeFaceNormals();
            let baseMaterial = new THREE.MeshLambertMaterial({
                color: 'white',
                side: THREE.DoubleSide,
                reflectivity:0.1,
                transparent: false
            });
            let firstBaseMesh = new THREE.Mesh(baseGeometry, baseMaterial);
            firstBaseMesh.renderOrder = -3;
            firstBaseMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            firstBaseMesh.position.set(64.25, 62.75, 0);
            firstBaseMesh.rotation.z = -Math.PI/4;
            pitchReleaseThreeD.fieldGroup.add(firstBaseMesh);

            let secondBaseMesh = new THREE.Mesh(baseGeometry, baseMaterial);
            secondBaseMesh.renderOrder = -3;
            secondBaseMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            secondBaseMesh.position.set(0, 125.24, 0);
            secondBaseMesh.rotation.z = Math.PI/4;
            pitchReleaseThreeD.fieldGroup.add(secondBaseMesh);

            let thirdBaseMesh = new THREE.Mesh(baseGeometry, baseMaterial);
            thirdBaseMesh.renderOrder = -3;
            thirdBaseMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            thirdBaseMesh.position.set(-65.2, 61.7, 0);
            thirdBaseMesh.rotation.z = Math.PI/4;
            pitchReleaseThreeD.fieldGroup.add(thirdBaseMesh);

            pitchReleaseThreeD.processPitches(totalsJson, selectedId, isInitial, isAverage, allPitchers, pitBreakdown);
        }

        function battingCircle() {
            let battingShape = new THREE.Shape();

            battingShape.moveTo(10, 0, 0);
            battingShape.absarc(0,0,10,0,2*Math.PI);
            let battingGeometry = new THREE.ShapeGeometry(battingShape, 24);
            //computeVertices(battingGeometry);

            let battingMaterial = new THREE.MeshLambertMaterial({
                color: 'rgb(139,69,19)',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0.1
            });
            let battingCircle = new THREE.Mesh(battingGeometry, battingMaterial);
            battingCircle.renderOrder = -4;
            battingCircle.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            battingCircle.position.copy(new THREE.Vector3(0, 0, 0));
            pitchReleaseThreeD.fieldGroup.add(battingCircle);

            batterBoxes();
        }

        function pitchingMound() {
            $('#moundData').text("20\'l x 18\'w @ 59\' from BOHP, 1ft rise @ apex");

            let pitchingMoundGeometry = new THREE.SphereGeometry(21, 128, 64, 0, Math.PI * 2, 0, Math.PI);
            pitchingMoundGeometry.mergeVertices();
            pitchingMoundGeometry.computeVertexNormals(true);
            pitchingMoundGeometry.computeFaceNormals();
            pitchReleaseThreeD.clipPlanes[0].visible = false;
            let pitchingMoundMaterial = new THREE.MeshLambertMaterial({
                color: 'rgb(139,69,19)',
                side: THREE.DoubleSide,
                transparent: false,
                clippingPlanes: [pitchReleaseThreeD.clipPlanes[0]],
                reflectivity:0.1
            });
            let pitchingMound = new THREE.Mesh(pitchingMoundGeometry, pitchingMoundMaterial);
            pitchingMound.renderOrder = -3;
            pitchingMound.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            pitchingMound.position.set(0, 57.5833333, -20);
            pitchReleaseThreeD.fieldGroup.add(pitchingMound);

            let pitchMoundOutPath = new THREE.Shape();
            pitchMoundOutPath.absellipse(0,57.5833333,9,10,0, Math.PI*2, false,0);
            let pitchMoundOutGeo = new THREE.ShapeGeometry( pitchMoundOutPath, 24);
            pitchMoundOutGeo.mergeVertices();
            pitchMoundOutGeo.computeVertexNormals(true);
            pitchMoundOutGeo.computeFaceNormals();
            let pitchMoundOutMat = new THREE.MeshLambertMaterial( {
                color: 'rgb(139,69,19)',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0
            } );
            let pitchMoundOutEllipse = new THREE.Mesh( pitchMoundOutGeo, pitchMoundOutMat );
            pitchMoundOutEllipse.renderOrder = -4;
            pitchMoundOutEllipse.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            pitchReleaseThreeD.fieldGroup.add(pitchMoundOutEllipse);

            moundPlate();
        }

        function moundPlate() {
            $('#moundPlateData').text("6\"l x 24\"w @ 60.5\' from BOHP, 10in above HP");
            let moundPlateShape = new THREE.Shape();

            moundPlateShape.moveTo(0, 0, 0);
            moundPlateShape.lineTo(2, 0, 0);
            moundPlateShape.lineTo(2, 0.5, 0);
            moundPlateShape.lineTo(0, 0.5, 0);
            moundPlateShape.lineTo(0, 0, 0);

            let moundPlateExtrusion = {
                steps: 2,
                depth: .2,
                bevelEnabled: false
            };

            let moundPlateGeometry = new THREE.ExtrudeGeometry(moundPlateShape, moundPlateExtrusion);
            moundPlateGeometry.mergeVertices();
            moundPlateGeometry.computeVertexNormals(true);
            moundPlateGeometry.computeFaceNormals();
            let moundPlateMaterial = new THREE.MeshLambertMaterial({
                color: 'white',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0
            });
            let moundPlateMesh = new THREE.Mesh(moundPlateGeometry, moundPlateMaterial);

            moundPlateMesh.renderOrder = -2;
            moundPlateMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            moundPlateMesh.position.set(-1, 59.083333, 0.83);

            pitchReleaseThreeD.fieldGroup.add(moundPlateMesh);

            /*  Helper to pinpoint correct mound plate placement, relative to back point of home plate
            let moundPlateHelperGeo = new THREE.PlaneGeometry(.1, .1, 12, 12);
            let moundPlateHelperMat = new THREE.MeshLambertMaterial( {
                color: new THREE.Color('#0000FF'),
                side: THREE.DoubleSide
            } );
            let moundPlateHelperQuad = new THREE.Mesh( moundPlateHelperGeo, moundPlateHelperMat );

            moundPlateHelperQuad.position.set(-1.5, 59.083333, 0.75);

            pitchReleaseThreeD.fieldGroup.add(moundPlateHelperQuad);
            */

            bases();
        }

        function outfieldGrass() {
            $('#centerFieldData').text('400ft from BOHP');

            let outfieldGrassShape = new THREE.Shape();
            outfieldGrassShape.moveTo(0, -20, 0);
            outfieldGrassShape.lineTo(237, 202, 0);
            outfieldGrassShape.absellipse(0, 205, 240, 195, 0, Math.PI);
            outfieldGrassShape.lineTo(0, -20, 0);

            let outfieldGrassGeometry = new THREE.ShapeGeometry(outfieldGrassShape, 24);
            //computeVertices( outfieldGrassGeometry);
            outfieldGrassGeometry.mergeVertices();
            outfieldGrassGeometry.computeVertexNormals(true);
            outfieldGrassGeometry.computeFaceNormals();

            let outfieldGrassMaterial = new THREE.MeshLambertMaterial({
                color: 'green',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0.1
            });
            let outfieldGrassMesh = new THREE.Mesh(outfieldGrassGeometry, outfieldGrassMaterial);

            outfieldGrassMesh.renderOrder = -10;
            outfieldGrassMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(outfieldGrassMesh);

            infieldDirt();
        }

        function infieldDirt() {

            let infieldDirtShape = new THREE.Shape();
            infieldDirtShape.moveTo(0, -5, 0);
            infieldDirtShape.lineTo(85, 76, 0);

            infieldDirtShape.absellipse(0, 84.5, 94, 75, 0, Math.PI);
            infieldDirtShape.lineTo(0, -5, 0);

            let infieldDirtGeometry = new THREE.ShapeGeometry(infieldDirtShape, 24);
            //computeVertices( infieldDirtGeometry);

            let infieldDirtMaterial = new THREE.MeshLambertMaterial({
                color: 'rgb(139,69,19)',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0.1
            });
            let infieldDirtMesh = new THREE.Mesh(infieldDirtGeometry, infieldDirtMaterial);

            infieldDirtMesh.renderOrder = -9;
            infieldDirtMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(infieldDirtMesh);

            infieldGrass();
        }

        function infieldGrass() {

            let infieldGrassShape = new THREE.Shape();
            infieldGrassShape.moveTo(0, 2, 0);
            infieldGrassShape.lineTo(63.575, 63.575, 0);
            infieldGrassShape.lineTo(0, 125.24, 0);
            infieldGrassShape.lineTo(-63.575, 63.575, 0);
            infieldGrassShape.lineTo(0, 2, 0);

            let infieldGrassGeometry = new THREE.ShapeGeometry(infieldGrassShape, 24);
            //computeVertices(infieldGrassGeometry);

            let infieldGrassMaterial = new THREE.MeshLambertMaterial({
                color: 'green',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0.1
            });
            let infieldGrassMesh = new THREE.Mesh(infieldGrassGeometry, infieldGrassMaterial);

            infieldGrassMesh.renderOrder = -8;
            infieldGrassMesh.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };

            pitchReleaseThreeD.fieldGroup.add(infieldGrassMesh);

            baseCircles();
        }

        function baseCircles() {
            /*
            let clippingHelpers = new THREE.Group();
            clippingHelpers.add(new THREE.PlaneHelper(pitchReleaseThreeD.clipPlanes[1], 100, 0x00ff00));
            clippingHelpers.add(new THREE.PlaneHelper(pitchReleaseThreeD.clipPlanes[2], 100, 0x0000ff));
            clippingHelpers.visible = true;
            */
            let baseCircleMaterial = new THREE.MeshLambertMaterial({
                color: 'rgb(139,69,19)',
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0.1,
                clippingPlanes: [pitchReleaseThreeD.clipPlanes[1],pitchReleaseThreeD.clipPlanes[2]],
            });

            let baseCircleGeometry = new THREE.CircleGeometry(5, 32);
            baseCircleGeometry.mergeVertices();
            baseCircleGeometry.computeVertexNormals(true);
            baseCircleGeometry.computeFaceNormals();

            let baseCircleFirst = new THREE.Mesh(baseCircleGeometry, baseCircleMaterial);
            baseCircleFirst.renderOrder = -7;
            baseCircleFirst.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            baseCircleFirst.position.copy(new THREE.Vector3(63.575, 63.575, 0));
            pitchReleaseThreeD.fieldGroup.add(baseCircleFirst);

            let baseCircleSecond = new THREE.Mesh(baseCircleGeometry, baseCircleMaterial);
            baseCircleSecond.renderOrder = -7;
            baseCircleSecond.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            baseCircleSecond.position.copy(new THREE.Vector3(0, 125.24, 0));
            pitchReleaseThreeD.fieldGroup.add(baseCircleSecond);

            let baseCircleThird = new THREE.Mesh(baseCircleGeometry, baseCircleMaterial);
            baseCircleThird.renderOrder = -7;
            baseCircleThird.onBeforeRender = function (renderer) {
                renderer.clearDepth();
            };
            baseCircleThird.position.copy(new THREE.Vector3(-63.575, 63.575, 0));
            pitchReleaseThreeD.fieldGroup.add(baseCircleThird);
            //pitchReleaseThreeD.fieldGroup.add(clippingHelpers);
            battingCircle();
        }
    },

    createGrid: function(type, start, vertSize, horizSize, step, color) {

        let geometryVert = new THREE.Geometry();
        let geometryHoriz = new THREE.Geometry();
        let material = new THREE.LineDashedMaterial({
            color: color,
            transparent: true,
            opacity: .2
        });

        switch (type){
            case 'xz':
                for ( let i = start; i <= vertSize; i += step) {
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, 0, i));
                    geometryVert.vertices.push(new THREE.Vector3(horizSize, 0, i));
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, 0, i));
                }
                for ( let i = -horizSize; i <= horizSize; i += step) {
                    geometryHoriz.vertices.push(new THREE.Vector3( i, 0, start ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, 0, vertSize ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, 0, start ));
                }
                break;
            case 'xy':
                for ( let i = start; i <= vertSize; i += step) {
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, i, 0));
                    geometryVert.vertices.push(new THREE.Vector3(horizSize, i, 0));
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, i, 0));
                }
                for ( let i = -horizSize; i <= horizSize; i += step) {
                    geometryHoriz.vertices.push(new THREE.Vector3( i, start, 0 ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, vertSize, 0 ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, start, 0 ));
                }
                break;
            case 'yz':
                for ( let i = start; i <= vertSize; i += step) {
                    geometryVert.vertices.push(new THREE.Vector3(0, start, i));
                    geometryVert.vertices.push(new THREE.Vector3(0, horizSize, i));
                    geometryVert.vertices.push(new THREE.Vector3(0, start, i));
                }
                for ( let i = start; i <= horizSize; i += step) {
                    geometryHoriz.vertices.push(new THREE.Vector3(0,  i, start));
                    geometryHoriz.vertices.push(new THREE.Vector3(0,  i, vertSize));
                    geometryHoriz.vertices.push(new THREE.Vector3(0,  i, start));
                }
                break;
        }

        return [new THREE.Line(geometryVert, material), new THREE.Line(geometryHoriz, material)];
    },

    addGrids: function(){
        let axesHelper = new THREE.AxesHelper( 150 );
        pitchReleaseThreeD.gridGroup.add( axesHelper );
        pitchReleaseThreeD.gridStorage.axes = axesHelper;

        let xyPlanes = pitchReleaseThreeD.createGrid('xy', -5, 150, 100, 5, 'red');
        let xzPlanes = pitchReleaseThreeD.createGrid('xz', 0, 50, 100, 5, 'blue');
        let yzPlanes = pitchReleaseThreeD.createGrid('yz', 0, 50, 150, 5, 'lime');

        pitchReleaseThreeD.gridGroup.add( xyPlanes[0] );
        pitchReleaseThreeD.gridGroup.add( xyPlanes[1] );
        pitchReleaseThreeD.gridStorage.xyPlanes = xyPlanes;
        pitchReleaseThreeD.gridGroup.add( xzPlanes[0] );
        pitchReleaseThreeD.gridGroup.add( xzPlanes[1] );
        pitchReleaseThreeD.gridStorage.xzPlanes = xzPlanes;
        pitchReleaseThreeD.gridGroup.add( yzPlanes[0] );
        pitchReleaseThreeD.gridGroup.add( yzPlanes[1] );
        pitchReleaseThreeD.gridStorage.yzPlanes = yzPlanes;

        pitchReleaseThreeD.scene.add(pitchReleaseThreeD.gridGroup);
    },

    finalize: async function() {
        pitchReleaseThreeD.addGrids();

        pitchReleaseThreeD.scene.add(pitchReleaseThreeD.fieldGroup);
        pitchReleaseThreeD.scene.background = new THREE.Color(0xCCCCCC);

        // controls
        pitchReleaseThreeD.controls = new THREE.OrbitControls(pitchReleaseThreeD.camera, pitchReleaseThreeD.renderer.domElement);
        pitchReleaseThreeD.controls.maxPolarAngle = (Math.PI*66)/128;

        //window.addEventListener('mousemove', pitchReleaseThreeD.onDocumentMouseMove, false);
        //window.addEventListener('resize', pitchReleaseThreeD.onWindowResize, false);

        pitchReleaseThreeD.controls.target = new THREE.Vector3(pitchReleaseThreeD.controlsTarget[0], pitchReleaseThreeD.controlsTarget[1], pitchReleaseThreeD.controlsTarget[2]);
        pitchReleaseThreeD.controls.enabled = false;
        pitchReleaseThreeD.camera.rotation.z = Math.PI;
        pitchReleaseThreeD.controls.enabled = true;
        pitchReleaseThreeD.camera.updateMatrixWorld();
        pitchReleaseThreeD.controls.update();

        pitchReleaseThreeD.startAnimation();
        pitchReleaseThreeD.animating = true;
        pitchReleaseThreeD.timeout = window.setTimeout(function () {
            pitchReleaseThreeD.stopAnimation();
        }, 300);
        $("canvas")
            .bind('wheel', function(){pitchReleaseThreeD.onMouseWheel();})
            .bind('mousemove', function(e){e.preventDefault();pitchReleaseThreeD.onDocumentMouseMove(e);})
            .bind('click', function(){
                let allMeasure = $('#allMeasurements');
                if(allMeasure.is(":hidden")){
                } else {
                    allMeasure.toggle();
                    let newWidth = $('#measurements').width();
                    $('#grids').css("left", Number(newWidth + 20) + "px");
                }
                pitchReleaseThreeD.onMouseClick();
            });

        $(window)
            .bind('keydown', function(e){
                //console.log("key down");
                let keyID = e.keyCode || e.which || e.key;
                if(pitchReleaseThreeD.arrowKeysMap.hasOwnProperty(keyID)){
                    e.preventDefault();
                }
                pitchReleaseThreeD.onKeyDown();
            })
            .bind('keyup', function(){pitchReleaseThreeD.onKeyUp();})
            .bind('resize', function(){pitchReleaseThreeD.onWindowResize();});

        $("#header").bind('click', function(){
            let allMeasure = $('#allMeasurements');
            if(allMeasure.is(":hidden")){
            } else {
                allMeasure.toggle();
                let newWidth = $('#measurements').width();
                $('#grids').css("left", Number(newWidth + 20) + "px");
            }
            pitchReleaseThreeD.onMouseClick();
        });

        window.onbeforeunload = pitchReleaseThreeD.cleanUp();
        pitchReleaseThreeD.removeGrids();
    },

    stopAnimation: async function(){
        if(pitchReleaseThreeD.animating) {
            let animationComplete = new Promise((resolve) => {
                resolve();
            });
            cancelAnimationFrame(pitchReleaseThreeD.animationFrame);
            await(animationComplete);
            pitchReleaseThreeD.animating = false;
            //console.log("animation stopped");
        }
    },

    startAnimation: function() {
        pitchReleaseThreeD.animationFrame = requestAnimationFrame(pitchReleaseThreeD.startAnimation);
        pitchReleaseThreeD.render();
    },

    onMouseClick: async function(){
        if((!pitchReleaseThreeD.moving && !pitchRelData.isCleaning) || pitchReleaseThreeD.timeout === undefined){
            pitchReleaseThreeD.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if(!pitchReleaseThreeD.animating) {
                pitchReleaseThreeD.animating = true;
                pitchReleaseThreeD.startAnimation();
            }
            if (pitchReleaseThreeD.timeout !== undefined) {
                window.clearTimeout(pitchReleaseThreeD.timeout);
            }
            pitchReleaseThreeD.timeout = window.setTimeout(function () {
                pitchReleaseThreeD.stopAnimation();
            }, 300);
            await(movingComplete);
            pitchReleaseThreeD.moving = false;
        }
    },

    onWindowResize: async function() {
        if((!pitchReleaseThreeD.moving && !pitchRelData.isCleaning) || pitchReleaseThreeD.timeout === undefined) {
            pitchReleaseThreeD.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if (!pitchReleaseThreeD.animating) {
                pitchReleaseThreeD.animating = true;
                pitchReleaseThreeD.startAnimation();
            }
            if (pitchReleaseThreeD.timeout !== undefined) {
                window.clearTimeout(pitchReleaseThreeD.timeout);
            }
            pitchReleaseThreeD.timeout = window.setTimeout(function () {
                pitchReleaseThreeD.stopAnimation();
            }, 300);
            await(movingComplete);
            pitchReleaseThreeD.moving = false;
        }
        let lessHeight = Number(window.innerHeight*.163);
        let newHeight = Number(window.innerHeight - lessHeight);
        let lessWidth = Number(window.innerWidth*.02);
        let newWidth = Number(window.innerWidth - lessWidth);
        //console.log("innerHeight: " + window.innerHeight);
        //console.log("innerWidth: " + window.innerWidth);
        //console.log("lessWidth: " + lessWidth);
        //console.log("newWidth: " + newWidth);
        pitchReleaseThreeD.camera.aspect = newWidth / newHeight;
        pitchReleaseThreeD.camera.left = -40 * pitchReleaseThreeD.camera.aspect / 2;
        pitchReleaseThreeD.camera.right = 40 * pitchReleaseThreeD.camera.aspect / 2;
        pitchReleaseThreeD.camera.top = 40 / 2;
        pitchReleaseThreeD.camera.bottom = -40 / 2;
        pitchReleaseThreeD.renderer.setSize(newWidth, newHeight);
        pitchReleaseThreeD.camera.updateProjectionMatrix();
        pitchReleaseThreeD.controls.update();
    },

    onMouseWheel: async function(){
        if((!pitchReleaseThreeD.moving && !pitchRelData.isCleaning) || pitchReleaseThreeD.timeout === undefined){
            pitchReleaseThreeD.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if(!pitchReleaseThreeD.animating) {
                pitchReleaseThreeD.animating = true;
                pitchReleaseThreeD.startAnimation();
            }
            if (pitchReleaseThreeD.timeout !== undefined) {
                window.clearTimeout(pitchReleaseThreeD.timeout);
            }
            pitchReleaseThreeD.timeout = window.setTimeout(function () {
                pitchReleaseThreeD.stopAnimation();
            }, 300);
            await(movingComplete);
            pitchReleaseThreeD.moving = false;
        }
    },

    onKeyUp: async function(){
        let movingComplete = new Promise((resolve) => {
            resolve();
        });
        if (pitchReleaseThreeD.timeout !== undefined) {
            window.clearTimeout(pitchReleaseThreeD.timeout);
        }
        pitchReleaseThreeD.timeout = window.setTimeout(function () {
            pitchReleaseThreeD.stopAnimation();
        }, 300);
        await(movingComplete);
        pitchReleaseThreeD.moving = false;
    },

    onKeyDown: async function(){
        if (pitchReleaseThreeD.timeout !== undefined) {
            window.clearTimeout(pitchReleaseThreeD.timeout);
        }
        if((!pitchReleaseThreeD.moving && !pitchRelData.isCleaning) || pitchReleaseThreeD.timeout === undefined){
            pitchReleaseThreeD.moving = true;
            if(!pitchReleaseThreeD.animating) {
                pitchReleaseThreeD.animating = true;
                pitchReleaseThreeD.startAnimation();
            }
        }
    },

    onDocumentMouseMove: async function(event) {
        if((!pitchReleaseThreeD.moving && !pitchRelData.isCleaning) || pitchReleaseThreeD.timeout === undefined) {
            pitchReleaseThreeD.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if (!pitchReleaseThreeD.animating) {
                pitchReleaseThreeD.animating = true;
                pitchReleaseThreeD.startAnimation();
            }
            if (pitchReleaseThreeD.timeout !== undefined) {
                window.clearTimeout(pitchReleaseThreeD.timeout);
            }
            pitchReleaseThreeD.timeout = window.setTimeout(function () {
                pitchReleaseThreeD.stopAnimation();
            }, 300);
            await(movingComplete);
            pitchReleaseThreeD.moving = false;
        }
        pitchReleaseThreeD.rect = pitchReleaseThreeD.renderer.domElement.getBoundingClientRect();
        pitchReleaseThreeD.mouse.x = ((event.clientX - pitchReleaseThreeD.rect.left) / (pitchReleaseThreeD.rect.right - pitchReleaseThreeD.rect.left)) * 2 - 1;
        pitchReleaseThreeD.mouse.y = -((event.clientY - pitchReleaseThreeD.rect.top) / (pitchReleaseThreeD.rect.bottom - pitchReleaseThreeD.rect.top)) * 2 + 1;
    },

    render: async function() {
        // find intersections
        pitchReleaseThreeD.raycaster.setFromCamera(pitchReleaseThreeD.mouse, pitchReleaseThreeD.camera);
        let guid, object;
        pitchReleaseThreeD.objects = pitchReleaseThreeD.pitchesGroup.children;
        pitchReleaseThreeD.intersects = pitchReleaseThreeD.raycaster.intersectObjects(pitchReleaseThreeD.objects);

        if (pitchReleaseThreeD.intersects.length > 0) {
            if (pitchReleaseThreeD.INTERSECTED !== pitchReleaseThreeD.intersects[0].object) {
                if (pitchReleaseThreeD.INTERSECTED) {
                    guid = pitchReleaseThreeD.INTERSECTED.guid;
                    pitchReleaseThreeD.guidMap[guid].material.opacity = 1.0;
                    pitchReleaseThreeD.guidMap[guid].material.transparent = false;
                }
                pitchReleaseThreeD.INTERSECTED = pitchReleaseThreeD.intersects[0].object;
                guid = pitchReleaseThreeD.INTERSECTED.guid;
                pitchReleaseThreeD.guidMap[guid].material.opacity = 1.0;
                pitchReleaseThreeD.guidMap[guid].material.transparent = false;
            } else {
                for (let i = 0; i < pitchReleaseThreeD.objects.length; i++) {
                    object = pitchReleaseThreeD.objects[i];
                    guid = object.guid;
                    if (object !== pitchReleaseThreeD.INTERSECTED) {
                        pitchReleaseThreeD.guidMap[guid].material.opacity = 0.1;
                        pitchReleaseThreeD.guidMap[guid].material.transparent = true;
                    }
                }
                guid = pitchReleaseThreeD.INTERSECTED.guid;
                pitchReleaseThreeD.guidMap[guid].material.opacity = 1.0;
                pitchReleaseThreeD.guidMap[guid].material.transparent = false;
                $("#pitchInfo").css("visibility", "visible");
                pitchReleaseThreeD.INTERSECTED.callback();
            }
        } else {
            if (pitchReleaseThreeD.INTERSECTED) {
                for (let i = 0; i < pitchReleaseThreeD.objects.length; i++) {
                    object = pitchReleaseThreeD.objects[i];
                    guid = object.guid;
                    pitchReleaseThreeD.guidMap[guid].material.opacity = 1.0;
                    pitchReleaseThreeD.guidMap[guid].material.transparent = true;
                }
                $("#pitchInfo").css("visibility", "hidden");
            }
            pitchReleaseThreeD.INTERSECTED = null;
        }

        pitchReleaseThreeD.renderer.render(pitchReleaseThreeD.scene, pitchReleaseThreeD.camera);
        $('#loadingDiv').hide();
    },

    removeGrids: async function(){
        let removingComplete = new Promise((resolve) => {
            resolve();
        });
        for (let a = pitchReleaseThreeD.gridGroup.children.length - 1; a >= 0; a--) {
            let tempGridObj = pitchReleaseThreeD.gridGroup.children[a];
            pitchReleaseThreeD.gridGroup.remove(tempGridObj);
        }
        await(removingComplete);
        if(!pitchReleaseThreeD.animating) {
            pitchReleaseThreeD.animating = true;
            pitchReleaseThreeD.startAnimation();
        }
        if (pitchReleaseThreeD.timeout !== undefined) {
            window.clearTimeout(pitchReleaseThreeD.timeout);
        }
        pitchReleaseThreeD.timeout = window.setTimeout(function () {
            pitchReleaseThreeD.stopAnimation();
        }, 300);
    },

    reAddGrids: async function(){
        let addingComplete = new Promise((resolve) => {
            resolve();
        });
        pitchReleaseThreeD.gridGroup
            .add(pitchReleaseThreeD.gridStorage.axes)
            .add(pitchReleaseThreeD.gridStorage.xyPlanes[0])
            .add(pitchReleaseThreeD.gridStorage.xyPlanes[1])
            .add(pitchReleaseThreeD.gridStorage.xzPlanes[0])
            .add(pitchReleaseThreeD.gridStorage.xzPlanes[1])
            .add(pitchReleaseThreeD.gridStorage.yzPlanes[0])
            .add(pitchReleaseThreeD.gridStorage.yzPlanes[1]);
        await(addingComplete);
        if(!pitchReleaseThreeD.animating) {
            pitchReleaseThreeD.animating = true;
            pitchReleaseThreeD.startAnimation();
        }
        if (pitchReleaseThreeD.timeout !== undefined) {
            window.clearTimeout(pitchReleaseThreeD.timeout);
        }
        pitchReleaseThreeD.timeout = window.setTimeout(function () {
            pitchReleaseThreeD.stopAnimation();
        }, 300);
    },

    disposeGrids: async function(){
        pitchRelData.isCleaning = true;
        let disposingComplete = new Promise((resolve) => {
            resolve();
        });
        for (let a = pitchReleaseThreeD.gridGroup.children.length - 1; a >= 0; a--) {
            let tempGridObj = pitchReleaseThreeD.gridGroup.children[a];
            pitchReleaseThreeD.gridGroup.remove(tempGridObj);
            tempGridObj.geometry.dispose();
            pitchReleaseThreeD.cleanMaterial(tempGridObj.material);
        }
        await(disposingComplete);
        pitchRelData.isCleaning = false;
    },

    disposePitches: async function(){
        pitchRelData.isCleaning = true;
        let disposingComplete = new Promise((resolve) => {
            resolve();
        });
        pitchReleaseThreeD.pitchesGroup.traverse(object => {
            if (!object.isMesh) return;

            pitchReleaseThreeD.scene.remove(object);
            object.geometry.dispose();

            if (object.material.isMaterial) {
                pitchReleaseThreeD.cleanMaterial(object.material)
            } else {
                // array of materials
                for (const material of object.material) pitchReleaseThreeD.cleanMaterial(material);
            }
            object = undefined;
        });
        for (let k = pitchReleaseThreeD.pitchesGroup.children.length - 1; k >= 0; k--) {
            pitchReleaseThreeD.pitchesGroup.remove(pitchReleaseThreeD.pitchesGroup.children[k]);
        }
        pitchReleaseThreeD.guidMap1 = {};
        pitchReleaseThreeD.guidMap2 = {};
        pitchReleaseThreeD.guidMap3 = {};
        pitchReleaseThreeD.guidMap4 = {};
        await(disposingComplete);
        pitchRelData.isCleaning = false;
    },

    cleanUp: async function() {
        //console.log('Cleaning Started!');
        let cleaningComplete = new Promise((resolve) => {
            resolve();
        });
        pitchReleaseThreeD.renderer.dispose();
        pitchReleaseThreeD.disposeGrids();

        pitchReleaseThreeD.scene.traverse(object => {
            if (!object.isMesh) return;

            object.geometry.dispose();

            if (object.material.isMaterial) {
                pitchReleaseThreeD.cleanMaterial(object.material)
            } else {
                // array of materials
                for (const material of object.material) pitchReleaseThreeD.cleanMaterial(material)
            }
        });
        await(cleaningComplete);
        //console.log('Cleaning Complete!');
        if(!pitchReleaseThreeD.initialClean){
            cancelAnimationFrame(pitchReleaseThreeD.animationFrame);
            pitchReleaseThreeD.animating = false;
        }
        pitchReleaseThreeD.initialClean = false;
    },

    cleanMaterial: function(material){
        material.dispose();

        // dispose textures
        for (const key of Object.keys(material)) {
            const value = material[key];
            if (value && typeof value === 'object' && 'minFilter' in value) {
                value.dispose()
            }
        }
    }
};